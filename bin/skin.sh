#!/bin/sh
#

APPLICATION_PATH=$(dirname $(dirname $(readlink -f $0)))
SOURCE_PATH="${APPLICATION_PATH}/skin"
TARGET_PATH="${APPLICATION_PATH}/public_html/css"
LESSC="/usr/bin/lessc"

if [ ! -x ${LESSC} ]
then
    LESSC=$(which lessc)
fi

if [ ! -x ${LESSC} ]
then
    LESSC="${HOME}/bin/lessc"
fi

if [ ! -x ${LESSC} ]
then
    echo "'lessc' not found."
    exit 1
fi

mk_skin () {
    echo "${1}.less -> ${1}.css"
    ${LESSC} -x "${SOURCE_PATH}/${1}.less" > "${TARGET_PATH}/${1}.css"
}

if [ -n "${1}" ]
then
    mk_skin ${1}
else
    cd ${SOURCE_PATH}

    for file in $(find . -maxdepth 1 -iname '*.less')
    do
        name=$(echo "$(basename ${file})" | cut -d'.' -f1)
        mk_skin ${name}
    done
fi

