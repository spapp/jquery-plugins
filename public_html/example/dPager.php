<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   jquery_plugins
 * @since     2015.02.26.
 */

ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL | E_STRICT | E_DEPRECATED);

$template     = realpath(dirname(__FILE__)) . '/dPager/section.phtml';
$layout       = realpath(dirname(__FILE__)) . '/dPager/layout.phtml';
$sectionCount = 11;
$limit        = 3;
$pageCount    = ceil(($sectionCount - 1) / $limit);
$page         = getPage($pageCount);
$offset       = $page * $limit;

if (true === isAjax()) {
    if ($_GET['page'] < 0 or $_GET['page'] > $pageCount) {
        header('HTTP/1.1 404 Not Found', true, 404);
    } else {
        echo getSections($offset, $limit, $sectionCount, $template);
    }
} else {
    include($layout);
}

function isAjax() {
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) and 'XMLHttpRequest' === $_SERVER['HTTP_X_REQUESTED_WITH']);
}

function getPage($max, $min = 0) {
    $page = $min;

    if (isset($_GET['page'])) {
        $page = (int)$_GET['page'] - 1;
    }

    if ($page < $min) {
        $page = $min;
    } elseif ($page >= $max) {
        $page = $max - 1;
    }

    return $page;
}

function hasMore($currentIndex, $pageMax, $sectionMax) {
    return ($currentIndex < $sectionMax and $currentIndex < $pageMax);
}

function getSections($offset, $limit, $sectionCount, $template) {
    $output = 'ddd';
    $i      = $offset;

    ob_start();

    while (hasMore($i, $offset + $limit, $sectionCount)) {
        include($template);
        ++$i;
    }

    $output = ob_get_contents();
    ob_clean();

    return $output;
}

?>















































