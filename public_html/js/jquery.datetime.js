/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.03.19.
 */
(function ($, window, document) {
    var PLUGIN_NAME = 'datetime';
    var TEMPLATES = {
        'default': [
            '<time>',
            '<span class="dayname"></span>',
            '<span class="month-day">',
            '<span class="month-short"></span><br>',
            '<span class="day"></span>',
            '</span>',
            '<span class="year"></span>',
            '</time>'
        ],
        'normal':  [
            '<time class="normal">',
            '<span class="day"></span>',
            '<span class="month-day">',
            '<span class="dayname"></span><br>',
            '<span class="year"></span>. ',
            '<span class="month"></span>',
            '</span>',
            '</time>'
        ]
    };

    var TEMPLATE_NAV_BUTTONS = [
        '<a class="nav-button nav-back" href="#back">&laquo;</a>',
        '<a class="nav-button nav-next" href="#next">&raquo;</a>'
    ];

    /**
     * Datetime class
     *
     * @param {jQuery|Element|string} aElement
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Datetime (aElement, aOptions) {
        var _this = this;
        this._element = $(aElement);
        this._options = aOptions;

        this._element.addClass('ui-datetime').html(this._getTemplate());
        this.setDate(this.getOption('date', this._element.attr('data-date')));

        $('time', this._element).click(this.onClick.bind(this));
        $('.nav-button', this._element).click(this.onNavClick.bind(this));
    }

    /**
     * @type {jQuery}
     * @private
     */
    Datetime.prototype._element = null;
    /**
     * @type {object}
     * @private
     */
    Datetime.prototype._options = null;
    /**
     * @type {Date}
     * @private
     */
    Datetime.prototype._date = null;

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Datetime.prototype.getOption = function Datetime_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     * Sets current date value and updatae ui
     *
     * @param {string|number|Date} aDate
     * @returns {Datetime}
     * @function
     */
    Datetime.prototype.setDate = function Datetime_prototype_setDate (aDate) {
        var date = aDate || new Date();
        var callback = this.getOption('onChange', function () {
        });

        if ('string' === typeof aDate || 'number' === typeof aDate) {
            date = new Date(aDate);
        }

        this._date = date;
        this.update();
        callback.call(this, $.Event('change'), this._date.format(this.getOption('dateFormat')));

        return this;
    };
    /**
     * Update ui
     *
     * @returns {Datetime}
     * @function
     */
    Datetime.prototype.update = function Datetime_prototype_update () {
        var datetime = [
            this._date.getFullYear(),
            this._date.getMonth() < 9 ? '0' + (this._date.getMonth() + 1) : this._date.getMonth() + 1,
            this._date.getDate() < 10 ? '0' + this._date.getDate() : this._date.getDate()
        ];

        this._element.find('.year').html(this._date.getFullYear())
            .end().find('.dayname').html(this._getDayName(this._date.getDay()))
            .end().find('.month').html(this._getMonthName(this._date.getMonth()))
            .end().find('.month-short').html(this._getMonthName(this._date.getMonth(), true))
            .end().find('.day').html(this._date.getDate())
            .end().find('time').attr('datetime', datetime.join('-'));

        return this;
    };
    /**
     * Returns day name
     *
     * @param {number} aIndex
     * @returns {*}
     * @function
     */
    Datetime.prototype._getDayName = function Datetime_prototype__getDayName (aIndex) {
        var days = this.getOption('days', []);

        return days[aIndex];
    };
    /**
     * Returns name of month
     *
     * @param {number} aIndex
     * @param {boolean} aShort
     * @returns {*}
     * @function
     */
    Datetime.prototype._getMonthName = function Datetime_prototype__getMonthName (aIndex, aShort) {
        var days;

        if (aShort) {
            days = this.getOption('monthsShort', []);
        } else {
            days = this.getOption('months', []);
        }

        return days[aIndex];
    };
    /**
     * UI onClick event handler
     *
     * @param {Event} aEvent
     * @returns {boolean}
     * @function
     */
    Datetime.prototype.onClick = function Datetime_prototype_onClick (aEvent) {
        var callback = this.getOption('onClick', function () {
        });

        if ('function' === typeof callback) {
            callback.call(this, aEvent);
        }

        return false;
    };
    /**
     * Navigation buttons on click event handler
     *
     * @param {Event} aEvent
     * @returns {boolean}
     * @function
     */
    Datetime.prototype.onNavClick = function Datetime_prototype_onNavClick (aEvent) {
        var step = 1;

        if ($(aEvent.target).hasClass('nav-back')) {
            step = -1;
        }

        this._date.setDate((this._date.getDate() + step));
        this.setDate(this._date);

        return false;
    };
    /**
     * Returns curent ui layout
     *
     * @returns {string}
     * @function
     */
    Datetime.prototype._getTemplate = function Datetime_prototype__getTemplate () {
        var template;

        if (this._element.attr('data-template')) {
            template = this._element.attr('data-template');
        } else {
            template = this.getOption('template', 'default');
        }

        if ('string' === typeof template) {
            if (TEMPLATES[template]) {
                template = TEMPLATES[template].join('');
            }
        } else {
            template = template.join('');
        }

        if (true === this.getOption('showNavButtons', false)) {
            template = TEMPLATE_NAV_BUTTONS.join('') + template;
        }

        return template;
    };

    /**
     * datetime plugin
     *
     * @param {object} aOptions
     * @param {*} aValue
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function (aOptions, aValue) {
        if ('string' === typeof aOptions) {
            if ('setDate' === aOptions) {
                this.each(function () {
                    this['data-plugin-' + PLUGIN_NAME].setDate(aValue);
                });

                return;
            }

            aOptions = {
                date: aOptions
            };
        }

        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            this['data-plugin-' + PLUGIN_NAME] = new Datetime(this, options);
        });
    };

    /**
     * datetime plugin default options
     *
     * @type {{template: string, showNavButtons: boolean, selector: string, days: string[], months: string[],
     *     monthsShort: string[]}}
     */
    $.fn[PLUGIN_NAME].defaults = {
        template:       'default',
        showNavButtons: true,
        selector:       '.ui-datetime-container',
        dateFormat:     'YYYY-MM-DD',
        days:           [
            'Vasárnap',
            'Hétfő',
            'Kedd',
            'Szerda',
            'Csütörtök',
            'Péntek',
            'Szombat'
        ],
        months:         [
            'Január',
            'Február',
            'Március',
            'Április',
            'Május',
            'Június',
            'Július',
            'Augusztus',
            'Szeptember',
            'Október',
            'November',
            'December'
        ],
        monthsShort:    [
            'Jan',
            'Feb',
            'Márc',
            'Ápr',
            'Máj',
            'Jún',
            'Júl',
            'Aug',
            'Szept',
            'Okt',
            'Nov',
            'Dec'
        ]
    };

    /**
     * datetime plugin static access
     *
     * @param {string|Element|jQuery} aSelector
     */
    $[PLUGIN_NAME] = function (aSelector) {
        var selector = aSelector || $.fn[PLUGIN_NAME].defaults.selector;
        $(selector)[PLUGIN_NAME]();

        $(document).on('DOMNodeInserted', function (event) {
            $(event.target).find(selector)[PLUGIN_NAME]();
        });
    };

}(jQuery, window, document));
