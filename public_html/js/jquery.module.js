/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.22.
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'module';
    var modules = {};

    /**
     * Module class
     *
     * @param aOptions
     * @constructor
     */
    function Module (aOptions) {
        this.setOptions(aOptions);
        this._components = this.getOption('components', {});
        this._methods = this.getOption('methods', {});

        var listeners = this.getOption('listeners', {});
        var isApplication = this.getOption('isApplication', false);
        var appName = isApplication ? this.getName() : this.getOption('application');
        var element;

        for (var selector in listeners) {
            if (true === isApplication && selector === appName) {
                element = $(this);
            } else if (appName && selector === appName) {
                element = $(this.getApplication());
            } else if ('document' === selector) {
                element = $(document);
            } else {
                element = $(selector);
            }

            for (var events in listeners[selector]) {
                element.on(events, listeners[selector][events].bind(this));
            }
        }

        for (var component in this._components) {
            this[createMethodName(component)] = this.getCmp.bind(this, component);
        }

        for (var name in this._methods) {
            this[name] = this._methods[name].bind(this);
        }

        this.init();
    }

    /**
     * Returns a module
     *
     * @param {string} aModuleName
     * @returns {Module}
     * @static
     * @function
     */
    Module.get = function Module_get (aModuleName) {
        return modules[aModuleName];
    };
    /**
     * Sets a module
     *
     * @param {string} aModuleName
     * @param {Module} aModule
     * @returns {Module}
     * @static
     * @function
     */
    Module.set = function Module_set (aModuleName, aModule) {
        modules[aModuleName] = aModule;

        return Module;
    };
    /**
     * Returns TRUE if the module is exists
     *
     * @param {string} aModuleName
     * @returns {boolean}
     * @function
     */
    Module.has = function Module_has (aModuleName) {
        return (aModuleName && aModuleName in modules);
    };

    /**
     * @type {object}
     * @private
     */
    Module.prototype._options = null;
    /**
     * @type {object}
     * @private
     */
    Module.prototype._components = null;
    /**
     * @type {null}
     * @private
     */
    Module.prototype._methods = null;
    /**
     * Module initialization
     *
     * @returns {Module}
     * @function
     */
    Module.prototype.init = function Module_prototype_init () {
        var init = this.getOption('onInit', function () {
        });
        init.call(this);
        this.emit('init');

        return this;
    };

    Module.prototype.call = function Module_prototype_call (aMethodName) {
        var args = Array.prototype.slice.call(arguments, 1);

        if ('function' === typeof this._methods[aMethodName]) {
            return this._methods[aMethodName].apply(this, args);
        }

        return null;
    };
    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Module.prototype.getOption = function Module_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     *
     * @param {Object} aOptions
     * @function
     */
    Module.prototype.setOptions = function Module_prototype_setOptions (aOptions) {
        this._options = aOptions;
    };
    /**
     * Emits an application event
     *
     * @param {string} aEventName
     * @param {*} aData
     * @function
     */
    Module.prototype.emit = function Module_prototype_emit (aEventName, aData) {
        var eventName = this.getName() + ':' + aEventName;

        $(this.getApplication()).trigger($.Event(eventName), aData);
    };
    /**
     * Returns aplication module
     *
     * @returns {Module|document}
     * @function
     */
    Module.prototype.getApplication = function Module_prototype_getApplication () {
        var appName;

        if (true === this.getOption('isApplication', false)) {
            appName = this.getName();
        } else {
            appName = this.getOption('application');
        }

        if (true === Module.has(appName)) {
            return Module.get(appName);
        }

        return document;
    };
    /**
     * Returns the module name
     *
     * @returns {string}
     * @function
     */
    Module.prototype.getName = function Module_prototype_getName () {
        return this.getOption('name');
    };
    /**
     * Returns a module component
     *
     * @param {string} aName
     * @returns {*}
     * @function
     */
    Module.prototype.getCmp = function Module_prototype_getCmp (aName) {
        return this._components[aName];
    };
    /**
     * Returns TRUE if the module component is exists
     *
     * @param {string} aNameaName
     * @returns {boolean}
     * @function
     */
    Module.prototype.hasCmp = function Module_prototype_hasCmp (aName) {
        return (aName && aName in this._components);
    };
    /**
     * Returns a valid method name
     *
     * @param {string} aText
     * @param {string} aPrefix
     * @returns {string}
     */
    function createMethodName (aText, aPrefix) {
        var prefix = aPrefix || 'get';
        var parts = aText.replace(/[^a-z0-9]/ig, ' ').replace(/ {2,}/ig, ' ').trim().split(' ');

        for (var i = 0; i < parts.length; i++) {
            parts[i] = parts[i].substr(0, 1).toUpperCase() + parts[i].substr(1);
        }

        return prefix + parts.join('');
    }

    /**
     * module plugin static access
     *
     * <code>
     *  $.module(
     *      {
     *          name:        'testModule',
     *          application: 'testApplication',
     *          components: {
     *              <name>: *
     *          },
     *          listeners: {
     *              testApplication: {
     *                  'app.int': function(){}
     *              },
     *              <selector>: {
     *                  <event>: function (){},
     *                  <event>: function (){}
     *              }
     *          }
     *      }
     *  );
     * </code>
     * @param {object} aOptions
     * @property {string}   aOptions.name REQUIRED
     * @property {boolean}  aOptions.isApplication
     * @property {string}   aOptions.application
     * @property {object}   aOptions.listeners
     * @property {object}   aOptions.listeners.<selector>
     * @property {function} aOptions.listeners.<selector>.<event>
     * @property {object}   aOptions.components
     * @property {jQuery}   aOptions.components.<name>
     * @returns {Module}
     */
    $[PLUGIN_NAME] = function jQuery_module (aOptions) {
        var allowed = [
            'get',
            'set',
            'has'
        ];

        if ('string' === typeof aOptions && $.inArray(aOptions, allowed) > -1) {
            var args = Array.prototype.slice.call(arguments, 1);

            Module[aOptions].call(undefined, args);
        } else if ('string' === typeof aOptions.name && aOptions.name.length > 3) {
            var module = new Module(aOptions);

            Module.set(aOptions.name, module);

            return module;
        }

        throw new Error('Name of the module is required '
                        + 'and must be longer than three characters.');
    };

    $[PLUGIN_NAME].get = function jQuery_module_get (aModuleName) {
        return Module.get(aModuleName);
    };

    $[PLUGIN_NAME].has = function jQuery_module_has (aModuleName) {
        return Module.has(aModuleName);
    };

}(jQuery, window, document));
