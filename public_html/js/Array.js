/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.23.
 * @external  Array
 */

if (!Array.isArray) {
    Array.isArray = function Array_isArray (aArgs) {
        return Object.prototype.toString.call(aArgs) === '[object Array]';
    };
}

