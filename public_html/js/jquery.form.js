/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   jquery_plugins
 * @since     2015.03.11.
 * @link      https://bitbucket.org/spapp/jquery_plugins
 * @requires  jQuery.email
 *
 * <code>
 *      $('form').form(
 *          {
 *              template: '#template-form1',
 *              values:{
 *                  text:'text',
 *                  radio: 'radio2'
 *              }
 *          }
 *      );
 * </code>
 *
 */
(function ($, window, document) {
    var PLUGIN_NAME = 'form';

    /**
     * Form class
     *
     * @param {jQuery|Element|string} aElement
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Form (aElement, aOptions) {
        var _this = this;
        this._element = $(aElement);
        this._options = aOptions;

        this._element
            .addClass(Form.STYLE_FORM)
            .on(
            'submit',
            function (event) {
                event.preventDefault();
                return _this.submit(event);
            })
            .on(
            'reset',
            function (event) {
                _this._element
                    .find('.' + Form.STYLE_FIELD_ERROR_LIST)
                    .remove()
                    .end()
                    .find('.' + Form.STYLE_FIELD_INVALID + ',.' + Form.STYLE_FIELD_VALID)
                    .removeClass(Form.STYLE_FIELD_VALID)
                    .removeClass(Form.STYLE_FIELD_INVALID);

            });

        this._refresh();
    }

    Form.STYLE_FORM = 'ui-form';
    Form.STYLE_GROUP = 'ui-form-group';
    Form.STYLE_GROUP_CLOSED = 'ui-form-group-closed';
    Form.STYLE_FIELD = 'ui-form-field';
    Form.STYLE_FIELD_ERROR_LIST = 'ui-form-field-error-list';
    Form.STYLE_FIELD_INVALID = 'ui-form-field-invalid';
    Form.STYLE_FIELD_VALID = 'ui-form-field-valid';
    Form.STYLE_FIELD_REQUIRED = 'ui-form-field-required';
    Form.STYLE_FIELD_DISABLED = 'ui-form-field-disabled';

    Form.GROUP_ANIMATION_DURATION = 'fast';

    Form.PATTERN_URL = /^(https?|ftp|ssl|file):/i;

    Form.DUMMY_FN = function () {
        return true
    };

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Form.prototype.getOption = function Form_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     * Refresh the form element
     *
     * @returns {Form}
     * @function
     */
    Form.prototype._refresh = function Form_prototype__refresh () {
        var formTpl = this.getOption('template');
        var _this = this;

        if (formTpl) {
            this.setTemplate(formTpl);
        }

        this._element.attr('novalidate', 'novalidate');
        this.setValues(this.getOption('values', {}));

        $('[name]', this._element).each(function () {
            var el = $(this);
            var elLabel = el.siblings('label');
            var parent = el.closest('.' + Form.STYLE_FIELD);

            if (el.is(':required')) {
                parent.addClass(Form.STYLE_FIELD_REQUIRED);
            } else if (el.is(':disabled')) {
                parent.addClass(Form.STYLE_FIELD_DISABLED);
            }

            if (!el.attr('id')) {
                el.attr('id', 'id_' + el.attr('name'));
            }

            // @todo radio group?
            if (!elLabel.attr('for')) {
                elLabel.attr('for', el.attr('id'));
            }

            if (true === _this.getOption('immediateChecking', false)) {
                el.blur(function (event) {
                    _this.isValid(this);
                });
            }
        });

        $('legend [type="checkbox"]', this._element).each(function () {
            var groupContainer = $(this).closest('.' + Form.STYLE_GROUP);

            groupContainer.attr('data-height', groupContainer.height());

            if (!$(this).is(':checked')) {
                animate(
                    1,
                    false,
                    groupContainer
                );
            }

            $(this).on('click', _this.toggleGroup.bind(_this, groupContainer));

        });

        return this;
    };
    /**
     * Oppen or close the group
     *
     * @param {string|Element} aGroupSelector
     * @returns {Form}
     * @function
     */
    Form.prototype.toggleGroup = function Form_prototype_toggleGroupt (aGroupSelector) {
        var group = $(aGroupSelector);
        var groupTagName = (group.attr('tagName') || '').toLowerCase();

        if ('input' === groupTagName) {
            group = group.closest('.' + Form.STYLE_GROUP);
        }

        group.removeClass(Form.STYLE_GROUP_CLOSED);

        animate(
            Form.GROUP_ANIMATION_DURATION,
            $('legend [type="checkbox"]', group).is(':checked'),
            group
        );

        return this;
    };

    /**
     * Submits the form
     *
     * @param {jQuery.Event} aEvent
     * @returns {boolean}
     * @function
     */
    Form.prototype.submit = function Form_prototype_submit (aEvent) {
        if (true === this.isValid()) {
            var beforeSend = this.getOption('beforeSend', Form.DUMMY_FN);

            if (false !== beforeSend.call(this, aEvent)) {
                $.ajax(
                    {
                        url:      (this._element.attr('action') || ''),
                        method:   (this._element.attr('method') || 'GET'),
                        data:     this.getValues(),
                        dataType: this.getOption('dataType'),
                        success:  this.getOption('success', Form.DUMMY_FN).bind(this),
                        error:    this.getOption('error', Form.DUMMY_FN).bind(this),
                        complete: this.getOption('complete', Form.DUMMY_FN).bind(this)
                    }
                );
            }
        }

        return false;
    };
    /**
     * Sets up the form template
     *
     * @param {string} aTemplate template selector
     * @returns {Form}
     * @function
     */
    Form.prototype.setTemplate = function Form_prototype_setTemplate (aTemplate) {
        var tpl = $(aTemplate).clone().get(0).content;

        this._element.append(tpl);

        return this;
    };
    /**
     * Sets up form values
     *
     * @param {object} aValues
     * @returns {Form}
     * @function
     */
    Form.prototype.setValues = function Form_prototype_setValues (aValues) {
        var _this = this;
        var names = Object.keys(aValues || {});

        for (var i = 0; i < names.length; i++) {
            this.setValue(names[i], aValues[names[i]]);
        }

        return this;
    };
    /**
     * Returns form values
     *
     * @param aForce
     * @returns {{}}
     * @function
     */
    Form.prototype.getValues = function Form_prototype_getValues (aForce) {
        var _this = this;
        var values = {};

        getInputByName('', this._element).each(function () {
            if (aForce || !$(this).is(':disabled')) {
                values[$(this).attr('name')] = _this.getValue(this);
            }
        });

        return values;
    };
    /**
     * Sets up a form item value
     *
     * @param {string} aName
     * @param {*} aValue
     * @returns {Form}
     * @function
     */
    Form.prototype.setValue = function Form_prototype_setValue (aName, aValue) {
        var item = getInputByName(aName, this._element);
        var type = item.attr('type') || item.get(0).tagName.toLowerCase();

        switch (type) {
            case 'checkbox':
                if (true === aValue || 'on' === aValue || 'yes' === aValue) {
                    item.attr('checked', 'checked');
                } else {
                    item.removeAttr('checked');
                }
                break;
            case 'radio':
                item.filter('[value="' + aValue + '"]').attr('checked', true);
                break;
            default:
                item.val(aValue);
        }

        return this;
    };
    /**
     * Returns a form item value
     *
     * @param {string} aName
     * @returns {*}
     * @function
     */
    Form.prototype.getValue = function Form_prototype_getValue (aName) {
        var item = getInputByName(aName, this._element);
        var value = null;

        switch (item.attr('type')) {
            case 'checkbox':
                value = item.is(':checked');
                break;
            case 'radio':
                var selector = '[name="' + item.attr('name') + '"]:checked';
                value = $(selector, this._element).val();
                break;
            default:
                value = item.val();
        }

        return value;
    };
    /**
     * Returns TRUE if the item is required
     *
     * @param {string|Element} aName
     * @returns {boolean}
     * @function
     */
    Form.prototype.isRequired = function Form_prototype_isRequired (aName) {
        return !!getInputByName(aName, this._element).attr('required');
    };
    /**
     * Returns TRUE if the form or the element is vontained a valid value
     *
     * @param {string|Element} aName
     * @returns {*}
     * @function
     */
    Form.prototype.isValid = function Form_prototype_isValid (aName) {
        var isValid;

        if (aName) {
            var el = getInputByName(aName, this._element);
            checkValidity(el, this);
            isValid = !el.parent().hasClass(Form.STYLE_FIELD_INVALID);
        } else {
            this.validate();
            isValid = ($('.' + Form.STYLE_FIELD_INVALID, this._element).length === 0);
        }

        return isValid;
    };
    /**
     * Checks validity of form
     *
     * @returns {Form}
     * @function
     */
    Form.prototype.validate = function Form_prototype_validate () {
        var _this = this;

        this._element.find('[name]').each(function () {
            checkValidity(this, _this);
        });

        return this;
    };
    /**
     * Checks validity of a form item
     *
     * @param {Element} aElement
     * @param {Form} aFormInstance
     */
    function checkValidity (aElement, aFormInstance) {
        var errors = [];
        var el = $(aElement);
        var pattern = el.attr('data-pattern') || '';

        el.parent()
            .removeClass(Form.STYLE_FIELD_INVALID)
            .removeClass(Form.STYLE_FIELD_VALID)
            .find('ul')
            .remove();

        function isValid (text, pattern) {
            var p = new RegExp(pattern, 'g');
            return p.test(text);
        }

        if (pattern) {
            var message = el.attr('data-message') || '';

            if (!isValid(el.val(), pattern)) {
                errors.push(message);
            }
        } else if ('function' === typeof el.get(0).checkValidity) {
            if (!el.get(0).checkValidity()) {
                errors.push(el.get(0).validationMessage);
            }
        } else if (true === aFormInstance.isRequired(el)) {
            errors.push(aFormInstance.getOption('errorRequired'));
        } else {
            var type = el.attr('type');

            if ('number' === type && !el.isNumeric()) {
                errors.push(aFormInstance.getOption('errorNumber'));
            } else if ('url' === type && !isValid(el.val(), Form.PATTERN_URL)) {
                errors.push(aFormInstance.getOption('errorUrl'));
            } else if ('email' === type && !$.email(el.val()).isValid()) {
                errors.push(aFormInstance.getOption('errorEmail'));
            }
        }

        if (errors.length > 0) {
            checkClosedGroup(el);

            el.parent().addClass(Form.STYLE_FIELD_INVALID);
            $('<ul class="'
              + Form.STYLE_FIELD_ERROR_LIST
              + '"><li>'
              + errors.join('</li><li>')
              + '</li></ul>')
                .insertAfter(el)
                .parent()
                .addClass(Form.STYLE_ERROR);
        } else if (aFormInstance.getOption('validityIndicator', false)) {
            el.parent().addClass(Form.STYLE_FIELD_VALID);
        }
    }

    /**
     *
     * @param {string|Element} aName
     * @param {jQuery|Element} aContext
     * @returns {*}
     */
    function getInputByName (aName, aContext) {
        var input = null;

        if ('string' === typeof aName) {
            if (aName) {
                input = $('[name="' + aName + '"]', aContext);
            } else {
                input = $('[name]', aContext);
            }
        } else {
            input = $(aName);
        }

        return input;
    }

    /**
     *
     * @param {*} aDuration
     * @param {boolean} aOpen
     * @param {string|Element} aContainer
     */
    function animate (aDuration, aOpen, aContainer) {
        var properties = {
            height: aContainer.attr('data-height')
        };
        var options = {
            duration: aDuration,
            complete: function () {
                aContainer[(aOpen ? 'removeClass' : 'addClass')](Form.STYLE_GROUP_CLOSED);

                if (aOpen) {
                    aContainer.css('height', 'auto').attr('data-height', aContainer.height());
                }
            }
        };

        if (!aOpen) {
            properties.height = '0';
        }

        aContainer.animate(properties, options);
    }

    /**
     *
     * @param aElement
     */
    function checkClosedGroup (aElement) {
        var checkbox = $('legend [type="checkbox"]', $(aElement).closest('.' + Form.STYLE_GROUP));

        if (checkbox && !checkbox.is(':checked')) {
            checkbox.trigger('click');
        }
    }

    /**
     * form plugin
     *
     * @param {object} aOptions
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function jQuery_prototype_form (aOptions) {
        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            this['data-plugin-' + PLUGIN_NAME] = new Form(this, options);
        });
    };

    /**
     * form plugin default options
     *
     * @type {{template: null, values: null, dataType: string, attr: {method: string, novalidate:
     *     string}, errorRequired: string, errorNumber: string, errorUrl: string, errorEmail: string, beforeSend:
     *     Function, success: Function, error: Function, complete: Function}}
     */
    $.fn[PLUGIN_NAME].defaults = {
        template:          null,
        values:            null,
        immediateChecking: true,
        validityIndicator: false,
        dataType:          'json',
        errorRequired:     'Kérjük, töltse ki ezt a mezőt.',
        errorNumber:       'Kérjük adjon meg egy számot.',
        errorUrl:          'Adjon meg egy URL-t.',
        errorEmail:        'Adjon meg egy e-mail címet.',
        beforeSend:        Form.DUMMY_FN,
        success:           Form.DUMMY_FN,
        error:             Form.DUMMY_FN,
        complete:          Form.DUMMY_FN

    };

}(jQuery, window, document));

