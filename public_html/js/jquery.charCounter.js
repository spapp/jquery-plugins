/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   jquery_plugins
 * @since     2015.02.18.
 */

(function ($) {
    var PLUGIN_NAME = 'charCounter';

    /**
     * @param {Element|string|jQuery} aElement
     * @param {object} aOptions
     * @constructor
     */
    function CharCounter (aElement, aOptions) {
        this._element = $(aElement);
        this._options = aOptions;

        this.getSourceEl().on('keyup keydown blur', this.display.bind(this)).trigger('keyup');
    }

    /**
     * Contains the instance config.
     *
     * @type {object}
     * @private
     */
    CharCounter.prototype._options = null;
    /**
     * Contains the displayed information.
     *
     * @type {jQuery}
     * @private
     */
    CharCounter.prototype._element = null;
    /**
     * The source element.
     * Typically an input item (textarea).
     *
     * @type {jQuery}
     * @private
     */
    CharCounter.prototype._sourceEl = null;
    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    CharCounter.prototype.getOption = function CharCounter_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     * Returns the source element.
     *
     * @see {@link CharCounter#_sourceEl}
     * @returns {null|jQuery}
     * @function
     */
    CharCounter.prototype.getSourceEl = function CharCounter_prototype_getSourceEl () {
        if (null === this._sourceEl) {
            var sourceAttr = this.getOption('sourceAttr');
            this._sourceEl = this._element.parent().find(this.getOption('source', this._element.attr(sourceAttr)));
        }

        return this._sourceEl;
    };
    /**
     * Calculates the source element text length and display it.
     *
     * @see {@link CharCounter#_sourceEl}
     * @see {@link CharCounter#_element}
     * @returns {CharCounter}
     * @function
     */
    CharCounter.prototype.display = function CharCounter_prototype_display () {
        var maxLengthAttr = this.getOption('maxLengthAttr');
        var template = this.getOption('template');
        var text = this.getSourceEl().val();
        var textLength = text.replace(/[\r\n]/ig, '').length + (text.replace(/[^\n]|/ig, '').length * 2);
        var data = {
            length:    parseInt(textLength, 10),
            maxlength: parseInt(this.getSourceEl().attr(maxLengthAttr), 10)
        };

        data.remaining = data.maxlength - data.length;

        if (data.remaining < 0) {
            data.remaining = 0;
        }

        if (data.length > data.maxlength) {
            this._element.addClass(this.getOption('errorClass'));
        } else {
            this._element.removeClass(this.getOption('errorClass'));
        }

        if (true === this.getOption('remaining', false)) {
            template = this.getOption('remainingTemplate');
        } else {
            template = this.getOption('template');
        }

        var html = template.replace(/\{([a-z]+)\}/gi, function (match, group) {
            return (group in data) ? data[group] : group;
        });

        this._element.html(html);

        return this;
    };
    /**
     * charCounter jQuery plugin
     *
     * @param {object} aOptions
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function jQuery_prototype_charCounter (aOptions) {
        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            new CharCounter(this, options);
        });
    };
    /**
     * charCounter jQuery plugin default settings
     *
     * @type {{selector: string, sourceAttr: string, errorClass: string, maxLengthAttr: string, template: string}}
     */
    $.fn[PLUGIN_NAME].defaults = {
        selector:          '.character-counter',
        sourceAttr:        'data-source',
        remaining:         false,
        errorClass:        'error',
        maxLengthAttr:     'maxlength',
        template:          '{length} / {maxlength}',
        remainingTemplate: '{remaining}'
    };

    /**
     * charCounter jQuery plugin static access
     *
     * @param {string|Element|jQuery} aSelector
     */
    $[PLUGIN_NAME] = function jQuery_charCounter (aSelector) {
        var selector = aSelector || $.fn[PLUGIN_NAME].defaults.selector;
        $(selector)[PLUGIN_NAME]();

        $(document).on('DOMNodeInserted', function (event) {
            $(event.target).find(selector)[PLUGIN_NAME]();
        });
    };
}(jQuery));