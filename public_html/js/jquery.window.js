/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.29.
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'window';

    var CLASS_NAME = 'ui-window';
    var CLASS_NAME_HELPER = 'ui-window-modal-helper';

    /**
     * Window class
     *
     * @param {jQuery|Element|string} aElement container element
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Window (aOptions) {
        this.setOptions(aOptions);
        this.update();
    }

    /**
     * @type {jQuery}
     * @private
     */
    Window.prototype._element = null;

    /**
     * @type {object}
     * @private
     */
    Window.prototype._options = null;
    /**
     * @type {Array}
     * @private
     */
    Window.prototype._buttons = null;
    /**
     * @type {jQuery}
     * @private
     */
    Window.prototype._body = null;

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Window.prototype.getOption = function Window_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     *
     * @param {Object} aOptions
     * @function
     */
    Window.prototype.setOptions = function Window_prototype_setOptions (aOptions) {
        this._options = aOptions;
    };

    /**
     * Updates window
     *
     * @todo too long method
     * @function
     */
    Window.prototype.update = function Window_prototype_update () {
        var button = '<button class="{0}" data-index="{1}">{2}</button>';
        var title = (this.getOption('title') || '&nbsp;');
        var height = this.getOption('height');
        var closable = this.getOption('closable');
        var template = $(this.getOption('template'));
        var body = (this.getOption('message') || '&nbsp;');
        var center = true === this.getOption('center') ? ' center' : '';
        var buttons = [];
        this._buttons = this.getOption('buttons');

        // 0 - title
        // 1 - body
        // 2 - buttons
        var html = [
            '<div class="' + CLASS_NAME + '">',
            '<div class="ui-window-title"><sapn>{0}</sapn>',
            (true === closable ? String.format(button, 'exit', 'exit', '×') : ''),
            '</div>',
            '<div class="ui-window-body' + center + '">{1}</div>',
            '<div class="ui-window-buttons">{2}</div>',
            '</div>'
        ];

        for (var i = 0; i < this._buttons.length; i++) {
            buttons.push(
                String.format(
                    button,
                    (this._buttons[i].type || ''),
                    i,
                    this._buttons[i].label
                )
            )
        }

        this._element = $(String.format(html.join(''), title, body, buttons.join('')))
            .addClass('ui-window')
            .addClass((buttons.length ? '' : 'no-buttons'))
            .addClass((this.getOption('noTitle') ? 'no-title' : ''))
            .attr('data-helper-id', String.uniqueId('helper'))
            .appendTo(document.body)
            .hide()
            .draggable({handle: '.ui-window-title'});

        if (height) {
            this.getBody().height(
                height
                - this._element.find('.ui-window-title').outerHeight()
                - this._element.find('.ui-window-buttons').outerHeight()
            );
        }

        if (template.length === 1) {
            this.getBody().empty().append(template.get(0).innerHTML);
        }

        if (true === this.getOption('modal')) {
            $('<div></div>')
                .attr('id', this._element.attr('data-helper-id'))
                .addClass(CLASS_NAME_HELPER)
                .height($(document).height())
                .appendTo(document.body);
        }

        setTimeout(this.toCenter.bind(this), 25);
        this._element.find('button').on('click', this.onButtonClick.bind(this));

        fireCallback(this, this.getOption('onShow'));

        return this;
    };

    Window.prototype.onButtonClick = function Window_prototype_onButtonClick (event) {
        var button = $(event.target);
        var index = button.attr('data-index');

        if ('exit' === index || !index) {
            this.close();
        } else {
            fireCallback(this, this._buttons[index].handler, event, this._buttons[index]);
        }

    };

    Window.prototype.getBody = function Window_prototype_getBody () {
        if (null === this._body) {
            this._body = this._element.find('.ui-window-body');
        }

        return this._body;
    };

    Window.prototype.close = function Window_prototype_close () {
        var _this = this;
        var win = this._element.closest('.' + CLASS_NAME);

        win.hide(
            'fast',
            function () {
                $('#' + _this._element.attr('data-helper-id')).remove();
                win.remove();
            }
        )

        return this;
    };

    Window.prototype.toCenter = function Window_prototype_toCenter () {
        var width = this.getOption('width', this._element.outerWidth());

        this._element.css(
            {
                width:      width,
                left:       '50%',
                top:        '50%',
                marginLeft: (-width / 2),
                marginTop:  (-this._element.outerHeight() / 2)
            }
        ).show('fast');

        return this;
    };

    /**
     * Fires a callback
     *
     * @param {object} aScope
     * @param {function} aCallback
     */
    function fireCallback (aScope, aCallback /*,arg0, arg1, ..., argN*/) {
        var args = Array.prototype.slice.call(arguments, 0);
        var scope = args.shift();
        var callback;

        if ('function' === typeof scope) {
            callback = scope;
            scope = undefined;
        } else {
            callback = args.shift();
        }

        if ('function' === typeof callback) {
            callback.apply(scope, args);
        }
    };

    /**
     * window plugin
     *
     * @param {object} aOptions
     * @property {function}  aOptions.onClick
     * @fires $.toolbar#click
     * @fires $.toolbar#click:<button name>
     * @function update ()
     * @function setButtons  (aButtons)
     * @returns {*}
     */
    $[PLUGIN_NAME] = function (aOptions) {
        var options = $.extend({}, $[PLUGIN_NAME].defaults, aOptions);

        if (!options.buttons) {
            options.buttons = [
                {
                    label:   options.labelOk,
                    type:    'ok',
                    handler: function (event, thisButton) {
                        this.close();
                    }
                }
            ];
        }

        return new Window(options);
    };

    $.windowCloseAll = function () {
        $('.' + CLASS_NAME).remove();
        $('.' + CLASS_NAME_HELPER).remove();
    };

    $.alert = function (aMessage, aTitle) {

        return $[PLUGIN_NAME](
            {
                message: aMessage,
                title:   (aTitle || ''),
                width:   300,
                center:  true,
                modal:   true
            }
        );
    };

    var progressCount = 0;

    $.progress = function (aMessage) {
        return $[PLUGIN_NAME](
            {
                message: aMessage,
                noTitle: true,
                width:   300,
                center:  true,
                modal:   true,
                buttons: []
            }
        );
    };

    $.confirm = function (aMessage, aTitle, aCallback) {
        var args = Array.prototype.slice.call(arguments, 0);
        var callback = args.pop();
        var message = args.shift();
        var title = args.length === 1 ? args[0] : '';

        return $[PLUGIN_NAME](
            {
                message: message,
                title:   title,
                width:   300,
                center:  true,
                modal:   true,
                buttons: [
                    {
                        label:   $[PLUGIN_NAME].defaults.labelNo,
                        handler: function (event, thisButton) {
                            this.close();
                        }
                    },
                    {
                        label:   $[PLUGIN_NAME].defaults.labelYes,
                        type:    'yes',
                        handler: function (event, thisButton) {
                            this.close();
                            callback.call(this, event, thisButton);
                        }
                    }
                ]
            }
        );
    };

    $[PLUGIN_NAME].defaults = {
        width:    300,
        title:    '',
        noTitle:  false,
        closable: true,
        message:  '',
        center:   false,
        modal:    true,
        template: null,
        labelOk:  'OK',
        labelYes: 'Igen',
        labelNo:  'Nem',
        buttons:  null
    };
}
(jQuery, window, document));
