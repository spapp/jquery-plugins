/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.15.
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'table';

    var FN_DUMMY = function (aEvent, aUi) {
    };

    function extend (aBase, aSuper) {
        aBase.__super = aSuper;

        aBase.prototype = Object.create(aSuper.prototype, {
            constructor: {
                value:        aBase,
                enumerable:   false,
                writable:     true,
                configurable: true
            }
        });
    }

    /**
     * Class OptionAbstract
     *
     * @param {object} aOptions
     * @constructor
     */
    function OptionAbstract (aOptions) {
        this.setOptions(aOptions);
    }

    /**
     * @type {object}
     * @private
     */
    OptionAbstract.prototype._options = null;

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    OptionAbstract.prototype.getOption = function OptionAbstract_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     *
     * @param {Object} aOptions
     * @function
     */
    OptionAbstract.prototype.setOptions = function OptionAbstract_prototype_setOptions (aOptions) {
        this._options = aOptions;
    };
    /**
     * Table class
     *
     * @param {jQuery|Element|string} aElement container element
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Table (aElement, aOptions) {
        this._element = $(aElement);

        Table.__super.call(this, aOptions);
        this.setData(this.getOption('data', []));
    }

    extend(Table, OptionAbstract);

    /**
     * @type {jQuery}
     * @private
     */
    Table.prototype._element = null;
    /**
     * @type {Array}
     * @private
     */
    Table.prototype._data = null;
    /**
     * Updates table
     *
     * @function
     */
    Table.prototype.update = function Table_prototype_update () {
        var _this = this;
        var callback = this.getOption('onLoad', function(){});
        var event = $.Event('load');
        var htmlTable = new HtmlTable(this._options, this._data);

        this._table = this._element.html(htmlTable.toString()).find('table');

        new DD(this._table, this._options);

        if (true === this.getOption('selectable', false)) {
            this._table.delegate('tr.row', 'click', function (event) {
                _this.selectRow(this);
            }).addClass('selectable');
        }

        callback.call(this, event)
    };
    /**
     * Adds a new record
     *
     * @param {object} aData
     * @returns {Table}
     * @function
     */
    Table.prototype.add = function Table_prototype_add (aData) {
        this._data.push(aData);
        this.update();

        return this;
    };
    /**
     * Insert a new record
     *
     * @param {object} aRecordData
     * @param {string} aBeforeKey
     * @param {*} aBeforeValue
     * @returns {Table}
     * @function
     */
    Table.prototype.insert = function Table_prototype_insert (aRecordData, aBeforeKey, aBeforeValue) {
        var beforeIndex;

        if ('number' === typeof aBeforeKey && 'undefined' === typeof aBeforeValue) {
            beforeIndex = aBeforeKey;
        } else {
            beforeIndex = this.indexOf(aBeforeKey, aBeforeValue);
        }

        this._data.splice(beforeIndex, 0, aRecordData);
        this.update();

        return this;
    };
    /**
     * Removes a record by a key
     *
     * @param aKey
     * @param aValue
     * @returns {Table}
     * @function
     */
    Table.prototype.remove = function Table_prototype_remove (aKey, aValue) {
        var index;

        if ('number' === typeof aKey && 'undefined' === typeof aValue) {
            index = aKey;
        } else {
            index = this.indexOf(aKey, aValue);
        }

        this._data.splice(index, 1);
        this.update();

        return this;
    };
    /**
     * Returns a record index by a key
     *
     * @param {string} aKey
     * @param {*} aValue
     * @returns {number}
     * @function
     */
    Table.prototype.indexOf = function Table_prototype_indexOf (aKey, aValue) {
        var index = null;

        for (var i = 0; i < this._data.length; i++) {
            if (this._data[i][aKey] === aValue) {
                index = i;
                break;
            }
        }

        return index;
    };
    /**
     * Finds and returns a record
     *
     * @param {string} aKey
     * @param {*} aValue
     * @returns {*}
     */
    Table.prototype.find = function Table_prototype_find (aKey, aValue) {
        var index = this.indexOf(aKey, aValue);
        var record = null;

        if (null !== index) {
            record = this._data[index];
        }

        return record;
    };
    /**
     * Clear all data
     *
     * @returns {Table}
     * @function
     */
    Table.prototype.clear = function Table_prototype_clear () {
        this.setData([]);

        return this;
    };
    /**
     * Returns all selected row data
     *
     * @returns {Array}
     * @function
     */
    Table.prototype.getSelected = function Table_prototype_getSelected () {
        var data = [];

        this._table.find('tr.row.selected').each(function (index, element) {
            data.push(JSON.parse(decodeURI($(element).data('data'))));
        });

        return data;
    };
    /**
     * Select a special row
     *
     * @param {number|string} aKey
     * @param {*} [aValue]
     * @returns {Table}
     * @function
     */
    Table.prototype.selectRow = function Table_prototype_selectRow (aKey, aValue) {
        var callback = this.getOption('onSelect');
        var row = aKey;

        if ('undefined' !== typeof aKey && 'undefined' !== typeof aValue) {
            row = this.indexOf(aKey, aValue);
        }

        if ('number' === typeof row) {
            var rows = this._table.find('tr.row');

            if (row >= 0 && row < rows.length) {
                row = rows.get(row);
            } else {
                throw new RangeError('Invalid row index.');
            }
        }

        row = $(row);

        if (!row.hasClass('disabled')) {
            var event = $.Event('select');
            var data = JSON.parse(decodeURI(row.data('data')));

            event.target = row.get(0);

            this._table.find('tr.row').removeClass('selected');
            row.addClass('selected');

            if ('function' === typeof callback) {
                callback.call(row.get(0), event, data);
            }

            this._element.trigger(event, data);
        }

        return this;
    };
    /**
     * Sets data
     *
     * @param {Array} aData
     * @returns {Table}
     */
    Table.prototype.setData = function (aData) {
        this._data = aData;

        this.update();

        return this;
    };
    Table.prototype.getData = function () {
        return JSON.parse(JSON.stringify(this._data));
    };
    /**
     * DD class
     *
     * @param {jQuery|Element|string} aElement container element
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function DD (aElement, aOptions) {
        this._element = $(aElement);
        DD.__super.call(this, aOptions);

        if (true === this.getOption('draggable', false)) {
            this._makeDraggable();
        }

        if (true === this.getOption('droppable', false)) {
            this._makeDroppable();

        }

        if (DD.directionHelper) {
            DD.directionHelper.remove();
        }

        DD.directionHelper = $(DD.TEMPLATE_DIRECTION_HELPER)
            .width(this._element.width() + 34)
            .css({left: this._element.offset().left + 'px'})
            .hide()
            .appendTo($('body'));

    }

    extend(DD, OptionAbstract);

    DD.DIRECTION_BEFORE = 'before';
    DD.DIRECTION_ON = 'on';
    DD.DIRECTION_AFTER = 'after';
    DD.TEMPLATE_DRAGGABLE_HELPER = '<div class="ui-table-draggable-helper"><table class="ui-table"></table></div>';
    DD.TEMPLATE_DIRECTION_HELPER = '<div class="ui-table-direction-helper">'
                                   + '<span class="left"></span>'
                                   + '<span class="right"></span></div>';
    /**
     * @type {jQuery}
     */
    DD.currentDroppable = null;
    /**
     * @type {string}
     */
    DD.currentDirection = null;
    /**
     * @type {jQuery}
     */
    DD.directionHelper = null;

    /**
     *
     */
    DD.clear = function () {
        DD.currentDroppable = null;
        DD.currentDirection = null;
        DD.directionHelper.hide();
    };
    /**
     *
     * @type {Element|jQuery}
     * @private
     */
    DD.prototype._element = null;
    /**
     * Show or hide direction highlight
     *
     * @param {jQuery|Element} [aTarget]
     * @param {string} [aDirection]
     * @private
     */
    DD.prototype._updateHighlight = function DD_prototype__updateHighlight (aTarget, aDirection) {
        var target = aTarget || this._element.find('tr.row');

        $(target)
            .removeClass('droppable-' + DD.DIRECTION_BEFORE)
            .removeClass('droppable-' + DD.DIRECTION_ON)
            .removeClass('droppable-' + DD.DIRECTION_AFTER);

        if (aTarget && aDirection && true !== this.getOption('noDirection')) {
            $(aTarget).addClass('droppable-' + aDirection);

            var targetLeft = aTarget.parent().offset().left - 16;

            if (DD.DIRECTION_ON === aDirection) {
                DD.directionHelper.hide();
            } else if (DD.DIRECTION_BEFORE === aDirection) {
                DD.directionHelper.css(
                    {
                        left: targetLeft + 'px',
                        top:  aTarget.offset().top - 8 + 'px'
                    }
                ).show();
            } else if (DD.DIRECTION_AFTER === aDirection) {
                DD.directionHelper.css(
                    {
                        left: targetLeft + 'px',
                        top:  aTarget.outerHeight() + aTarget.offset().top - 8 + 'px'
                    }
                ).show();
            }
        }
    };
    /**
     * Sets and returns drop direction
     *
     * @param {jQuery|Element} aTarget
     * @param {object} aUi
     * @returns {string}
     */
    DD.prototype.getDirection = function DD_prototype_getDirection (aTarget, aUi) {
        if ('undefined' !== typeof aTarget && 'undefined' !== typeof aUi) {
            this._currentDirection = this._calculateDirection(aTarget, aUi);
        }

        return this._currentDirection;
    };
    /**
     * Calculate and returns drop direction
     *
     * @param {jQuery|Element} aTarget
     * @param {object} aUi
     * @private
     * @returns {string}
     */
    DD.prototype._calculateDirection = function DD_prototype__calculateDirection (aTarget, aUi) {
        var target = $(aTarget);
        var direction = DD.DIRECTION_ON;
        var areaHeight = target.outerHeight() / 3;
        var helperTop = aUi.helper.offset().top + aUi.helper.outerHeight() - 7;
        var targetTop = target.offset().top;

        if (true === this.getOption('noDirection')) {
            direction = DD.DIRECTION_ON;
        } else if (helperTop <= targetTop + areaHeight) {
            direction = DD.DIRECTION_BEFORE;
        } else if (helperTop >= targetTop + 2 * areaHeight) {
            direction = DD.DIRECTION_AFTER;
        }

        return direction;
    };
    /**
     * Fires a callback function
     *
     * @param {function} aCallback
     * @param {Event} aEvent
     * @param {object} aData
     * @private
     *
     * @function
     */
    DD.prototype._fireCallback = function DD_prototype__fireCallback (aCallback, aEvent, aData) {
        if ('function' === typeof aCallback) {
            aCallback.call(this, aEvent, aData);
        }
    };
    /**
     * Droppable initialization
     *
     * @private
     */
    DD.prototype._makeDroppable = function DD_prototype__makeDroppable () {
        var _this = this;

        this._element.find('tr.row:not(.disabled)').droppable(
            {
                scope:       this.getOption('ddScope'),
                activeClass: 'active',
                hoverClass:  'hover',
                tolerance:   'pointer',
                over:        function (event, ui) {
                    DD.currentDroppable = $(this);

                    _this._updateHighlight(DD.currentDroppable, _this.getDirection(this, ui));
                    _this._fireCallback(_this.getOption('onDropOver'), event);
                    _this._element.trigger(event);
                },
                out:         function (event, ui) {
                    _this._updateHighlight();
                    _this._fireCallback(_this.getOption('onDropOut'), event);
                    _this._element.trigger(event);
                },
                drop:        function (event, ui) {
                    var tr = ui.helper.find('tr');

                    _this._updateHighlight();

                    var data = {
                        direction: ui.helper.currentDirection,
                        target:    {
                            element: DD.currentDroppable,
                            data:    JSON.parse(decodeURI(DD.currentDroppable.data('data')))
                        },
                        source:    {
                            element: tr,
                            data:    JSON.parse(decodeURI(tr.data('data')))
                        }
                    };

                    event.data = data;

                    _this._fireCallback(_this.getOption('onDrop'), event, data);
                    //_this._element.trigger($.Event('table:drop'), data);

                    DD.clear();

                    return false;
                }
            }
        );
    };
    /**
     * Draggable initialization
     *
     * @private
     */
    DD.prototype._makeDraggable = function DD_prototype__makeDraggable () {
        var _this = this;

        this._element.find('tr.row:not(.disabled)').bind('mousedown', function () {
            _this._element.disableSelection();
        }).bind('mouseup', function () {
            _this._element.enableSelection();
        }).draggable(
            {
                helper:   function (event) {
                    return $(DD.TEMPLATE_DRAGGABLE_HELPER)
                        .find('table')
                        .append($(event.target).closest('tr')
                                    .clone())
                        .end()
                        .insertAfter(_this._element);
                },
                cursorAt: {
                    left:   -10,
                    bottom: 5
                },
                //cursor:   'move',
                distance: 10,
                delay:    100,
                scope:    this.getOption('ddScope'),
                revert:   'invalid',
                drag:     function (event, ui) {
                    if (DD.currentDroppable) {
                        _this._updateHighlight(DD.currentDroppable, _this.getDirection(DD.currentDroppable, ui));
                    }

                    _this._fireCallback(_this.getOption('onDrag'), event);
                    _this._element.trigger(event);
                },
                stop:     function (event, ui) {

                    if (DD.currentDroppable) {
                        _this._updateHighlight(DD.currentDroppable);
                    }

                    _this._fireCallback(_this.getOption('onDragStop'), event);
                    _this._element.trigger(event);
                }
            }
        );
    };

    /**
     * HtmlTable
     *
     * @param aOptions
     * @constructor
     */
    function HtmlTable (aOptions, aData) {
        HtmlTable.__super.call(this, aOptions);
        this._data = aData || this.getOption('data', []);

        this.setHeaders(this.getOption('headers', []));
    }

    extend(HtmlTable, OptionAbstract);

    HtmlTable.prototype._headers = null;
    HtmlTable.prototype._data = null;
    /**
     * Prepares headers
     *
     * @param {Array} aHeaders
     * @returns {HtmlTable}
     * @function
     */
    HtmlTable.prototype.setHeaders = function HtmlTable_prototype_setHeaders (aHeaders) {
        this._headers = {};

        for (var i = 0; i < aHeaders.length; i++) {
            this._headers[aHeaders[i].name] = aHeaders[i];
        }

        return this;
    };
    /**
     * Returns a html table row
     *
     * @param {Object} aRowData
     * @param {boolean} aOdd
     * @returns {string}
     * @private
     */
    HtmlTable.prototype._getRow = function (aRowData, aOdd) {
        var rowData = aRowData.data || aRowData;
        var html = [
            '<tr ',
            'data-data="',
            encodeURI(JSON.stringify(rowData)),
            '" class="row',
            (aOdd ? ' odd' : ''),
            (aRowData.disabled && true === aRowData.disabled ? ' disabled' : ''),
            '">'
        ];
        var value;

        for (var name in this._headers) {
            value = '';

            if (name in rowData) {
                value = rowData[name];
            } else if (this._headers[name].default) {
                value = this._headers[name].default;
            }

            if ('function' === typeof this._headers[name].format) {
                value = this._headers[name].format.call(this._headers[name], value, rowData);
            }

            html.push('<td>', value, '</td>');
        }

        html.push('</tr>');

        return html.join('');
    }
    /**
     * Returns a html table headers
     *
     * @returns {string}
     * @private
     */
    HtmlTable.prototype._getHeader = function HtmlTable_prototype__getHeader () {
        var html = [
            '<thead>',
            '<tr>'
        ];

        for (var name in this._headers) {
            html.push('<th>', (this._headers[name].label || this._headers[name].name), '</th>');
        }

        html.push('</tr>', '</thead>');

        return html.join('');
    }
    /**
     * Returns html table
     *
     * @returns {string}
     */
    HtmlTable.prototype.toString = function HtmlTable_prototype_toString () {
        var html = ['<table class="ui-table">'];
        var caption = this.getOption('caption', '');

        if (caption) {
            html.push('<caption>', caption, '</caption>');
        }

        if (true === this.getOption('showHeader', true)) {
            html.push(this._getHeader());
        }

        html.push('<tbody>')

        for (var i = 0; i < this._data.length; i++) {
            html.push(this._getRow(this._data[i], (i % 2)));
        }

        html.push('</tbody>', '</table>');

        return html.join('');
    }

    /**
     * table plugin
     *
     * @param {object} aOptions
     * @fires $.table#select
     * @fires $.table#drag
     * @fires $.table#dragstop
     * @fires $.table#dropover
     * @fires $.table#dropout
     * @fires $.table#drop
     * @function add (aData)
     * @function insert (aRecordData, aBeforeKey, aBeforeValue)
     * @function remove  (aKey, aValue)
     * @function indexOf  (aKey, aValue)
     * @function find  (aKey, aValue)
     * @function selectRow  (aKey, aValue)
     * @function setData (aData)
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function (aOptions) {
        var allowed = [
            'add',
            'insert',
            'remove',
            'indexOf',
            'find',
            'selectRow',
            'setData',
            'getSelected'
        ];

        if ('string' === typeof aOptions && $.inArray(aOptions, allowed) > -1) {
            var args = Array.prototype.slice.call(arguments, 1);

            return this.each(
                function () {
                    var plugin = this['data-plugin-' + PLUGIN_NAME];
                    plugin[aOptions].apply(plugin, args);
                }
            );
        }

        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            this['data-plugin-' + PLUGIN_NAME] = new Table(this, options);
        });
    };
    /**
     * table plugin default options
     *
     * @type {{headers: {}, showHeader: boolean, selectable: boolean, draggable: boolean, droppable: boolean, ddScope:
     *     string, noDirection: boolean, onSelect: Function, onDrag: Function, onDragStop: Function, onDropOver:
     *     Function, onDropOut: Function, onDrop: Function}}
     */
    $.fn[PLUGIN_NAME].defaults = {
        headers:     {},
        showHeader:  true,
        selectable:  true,
        draggable:   false,
        droppable:   false,
        ddScope:     'ui-table',
        noDirection: true,
        onSelect:    FN_DUMMY,
        onDrag:      FN_DUMMY,
        onDragStop:  FN_DUMMY,
        onDropOver:  FN_DUMMY,
        onDropOut:   FN_DUMMY,
        onDrop:      FN_DUMMY,
        onLoad:      FN_DUMMY
    };
}(jQuery, window, document));
