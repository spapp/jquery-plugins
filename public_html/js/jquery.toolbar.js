/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.21.
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'toolbar';

    var FN_DUMMY = function (aEvent) {
    };

    /**
     * Toolbar class
     *
     * @param {jQuery|Element|string} aElement container element
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Toolbar (aElement, aOptions) {
        this.setOptions(aOptions);
        this._element = $(aElement).addClass('ui-toolbar').addClass(this.getOption('className', ''));
        this.setButtons(this.getOption('buttons', []));
    }

    /**
     * @type {jQuery}
     * @private
     */
    Toolbar.prototype._element = null;

    /**
     * @type {object}
     * @private
     */
    Toolbar.prototype._options = null;
    /**
     * @type {Array}
     * @private
     */
    Toolbar.prototype._buttons = null;

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Toolbar.prototype.getOption = function Toolbar_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     *
     * @param {Object} aOptions
     * @function
     */
    Toolbar.prototype.setOptions = function Toolbar_prototype_setOptions (aOptions) {
        this._options = aOptions;
    };
    /**
     *
     * @param aButtons
     * @function
     */
    Toolbar.prototype.setButtons = function Toolbar_prototype_setButtons (aButtons) {
        this._buttons = aButtons;

        setTimeout(this.update.bind(this), 50);
    };

    /**
     * Updates toolbar
     *
     * @todo too long method
     * @function
     */
    Toolbar.prototype.update = function Toolbar_prototype_update () {
        var _this = this;
        var html = [];
        var tpl = this.getOption('buttonTemplate');
        var iconStatus = this.getOption('icons');
        var type = this.getOption('type');
        var buttonSelector = 'a';
        var showIcon = true;
        var button, value;
        var attrs = [
            'name',
            'className',
            'label',
            'content'
        ];

        switch (this.getOption('icons')) {
            case 'only':
                this._element.addClass('only-icons');
                break;
            case 'hide':
                showIcon = false;
                break;
            case 'show':
            default:
        }

        switch (type) {
            case 'plain':
                showIcon = false;
                this._element.addClass('plain-text');
                break;
            case 'radio':
                this._element.addClass('radio');
                break;
            case 'tab':
                this._element.addClass('radio tab');
                break;
            case 'default':
            default:
        }

        for (var i = 0; i < this._buttons.length; i++) {
            button = tpl;

            if ('string' === typeof this._buttons[i]) {
                html.push(this._buttons[i]);
                continue;
            }

            if ('tab' === type) {
                $('#' + this._buttons[i].content).addClass('tab-content').hide();
            }

            for (var j = 0; j < attrs.length; j++) {
                value = '';

                if ('content' === attrs[j]) {
                    value = this._buttons[i].name;
                }

                if ('undefined' !== typeof this._buttons[i][attrs[j]]) {
                    value = this._buttons[i][attrs[j]];
                }

                if ('className' === attrs[j]) {
                    if (true === showIcon && 'string' === typeof this._buttons[i].icon) {
                        value += ' icon icon-' + this._buttons[i].icon;
                    }

                    if ('boolean' === typeof this._buttons[i].disabled) {
                        value += ' disabled';
                    }

                    if (i === 0) {
                        value += ' first-child';
                    }

                    if (i === this._buttons.length - 1) {
                        value += ' last-child';
                    }
                }

                button = button.replace('{' + attrs[j] + '}', value);
            }

            html.push(button);
        }

        if ('tab' === type) {
            html.push('<hr>');
            //var content = this._element.find('.tab-content');
            //buttonSelector = '.tab-toolbar a';
            //
            //if (content.length > 0) {
            //    content = content.html();
            //} else {
            //    content = this._element.html();
            //}
            //
            //html = [
            //    '<div class="tab-toolbar">',
            //    html.join(''),
            //    '</div>',
            //    '<div class="tab-content">',
            //    content,
            //    '</div>'
            //];
        }

        this._element.html(html.join('')).find(buttonSelector).addClass('toolbar-button');

        this._element.find(buttonSelector).click(function (event) {
            if (false === $(this).hasClass('disabled')) {
                _this.onClick(event, this);

                if ('tab' === type || 'radio' === type) {
                    _this.select($(this).attr('name'));
                }
            }

            return false;
        });

        if (('tab' === type || 'radio' === type) && 'number' === typeof this.getOption('selected')) {
            this.select(this.getOption('selected'));
        }

        fireCallback(this._element, this.getOption('onUpdate'), $.Event('update'), this);

        return this;
    };
    /**
     * Toolbar button onclick handler
     *
     * @param {Event} aEvent
     * @param {Element} aScope
     * @function
     */
    Toolbar.prototype.onClick = function Toolbar_prototype_onClick (aEvent, aScope) {
        var current = $((aScope || aEvent.target));
        var data = {
            element: current,
            name:    current.attr('name')
        };
        var event = $.Event('click:' + data.name);

        event.originalEvent = aEvent;
        fireCallback(current.get(0), this.getOption('onClick'), aEvent, data);

        this._element.trigger(aEvent, data);
        this._element.trigger(event, data);
    };
    /**
     * Clears all button
     *
     * @returns {Toolbar}
     */
    Toolbar.prototype.clear = function Toolbar_prototype_clear () {
        this._buttons = [];
        this.update();

        return this;
    };
    /**
     * Add a new toolbar button
     *
     * @param {object} aButton
     * @property {string}  aButton.name
     * @property {string}  aButton.label
     * @property {string}  aButton.icon
     * @property {boolean} aButton.disabled
     * @property {string}  aButton.content - content selector
     * @returns {Toolbar}
     */
    Toolbar.prototype.add = function Toolbar_prototype_add (aButton) {
        this._buttons.push(aButton);
        this.update();

        return this;
    };
    /**
     * Removes a button
     *
     * @param {string|number|Element|jQuery} aName
     * @returns {Toolbar}
     */
    Toolbar.prototype.remove = function Toolbar_prototype_remove (aName) {
        var index;

        if ('number' === typeof aName) {
            index = aName;
        } else {
            index = this.indexOf(aName);
        }

        this._buttons.splice(index, 1);
        this.update();

        return this;
    };

    /**
     * Returns a record index by a key
     *
     * @param {string|Element|jQuery} aName
     * @returns {number}
     * @function
     */
    Toolbar.prototype.indexOf = function Toolbar_prototype_indexOf (aName) {
        var index = null;

        if ('string' !== typeof aName) {
            aName = $(aName).attr('name');
        }

        for (var i = 0; i < this._buttons.length; i++) {
            if ('string' !== typeof this._buttons[i] && this._buttons[i].name === aName) {
                index = i;
                break;
            }
        }

        return index;
    };
    /**
     * Disable a button
     *
     * @param {string|number|Element|jQuery} aName
     * @returns {Toolbar}
     */
    Toolbar.prototype.disable = function Toolbar_prototype_disable (aName, aDisable) {
        var disable = false === aDisable ? false : true;
        var action = disable ? 'add' : 'remove';
        var selector;

        switch (typeof aName) {
            case 'number':
                selector = ':nth-child(' + aName + ')';
                break;
            case 'string':
                selector = '[name="' + aName + '"]';
                break;
            default:
                selector = aName;
        }

        var element = this._element.find(selector)[action + 'Class']('disabled');
        var event = jQuery.Event('disable');
        var data = {
            element: element,
            disable: disable,
            name:    element.attr('name')
        };

        fireCallback(element.get(0), this.getOption('onDisable'), event, data);
        this._element.trigger(event, data);

        if (element.hasClass('selected')) {
            this.select(this._element.find('a:not(.disabled):first'));
        }

        return this;
    };
    /**
     * Selects a button
     *
     * @param {string|number|Element|jQuery} aName
     * @returns {Toolbar}
     */
    Toolbar.prototype.select = function Toolbar_prototype_select (aName) {
        var index;
        var data;

        if ('number' === typeof aName) {
            index = aName;
        } else {
            index = this.indexOf(aName);
        }

        var oldSelected = this._element.find('a.selected');
        var newSelected = this._element.find('a:eq(' + index + ')');

        if (1 === newSelected.length
            && false === newSelected.hasClass('disabled')
            && false === newSelected.hasClass('selected')
        ) {

            oldSelected.removeClass('selected');
            newSelected.addClass('selected');

            if ('tab' === this.getOption('type')) {
                $(oldSelected.attr('href')).hide();
                $(newSelected.attr('href')).show();
            }

            var event = $.Event('select');
            data = {
                old:     {
                    element: oldSelected,
                    name:    oldSelected.attr('name')
                },
                current: {
                    element: newSelected,
                    name:    newSelected.attr('name')
                }
            };

            fireCallback(newSelected.get(0), this.getOption('onSelect'), event, data);
            this._element.trigger(event, data);
        }

        return this;
    };
    /**
     * Returns selected item name
     *
     * @returns {*}
     * @function
     */
    Toolbar.prototype.value = function Toolbar_prototype_value () {
        var selected = this._element.find('a.selected');

        if (selected.length > 0) {
            return selected.attr('name');
        }

        return null;
    };
    /**
     * Fires a callback
     *
     * @param {object} aScope
     * @param {function} aCallback
     */
    function fireCallback (aScope, aCallback /*,arg0, arg1, ..., argN*/) {
        var args = Array.prototype.slice.call(arguments, 0);
        var scope = args.shift();
        var callback;

        if ('function' === typeof scope) {
            callback = scope;
            scope = undefined;
        } else {
            callback = args.shift();
        }

        if ('function' === typeof callback) {
            callback.apply(scope, args);
        }
    };

    /**
     * toolbar plugin
     *
     * @param {object} aOptions
     * @property {enum}      aOptions.icons
     * @property {enum}      aOptions.type
     * @property {string}    aOptions.buttonTemplate
     * @property {function}  aOptions.onSelect
     * @property {function}  aOptions.onDisable
     * @property {function}  aOptions.onClick
     * @fires $.toolbar#select
     * @fires $.toolbar#disable
     * @fires $.toolbar#click
     * @fires $.toolbar#click:<button name>
     * @function clear ()
     * @function add (aData)
     * @function remove  (aName)
     * @function indexOf  (aName)
     * @function select  (aName)
     * @function disable  (aName)
     * @function value  ()
     * @function setButtons  (aButtons)
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function (aOptions) {
        var allowed = [
            'clear',
            'add',
            'remove',
            'indexOf',
            'select',
            'disable',
            'value',
            'setButtons'
        ];

        if ('string' === typeof aOptions && $.inArray(aOptions, allowed) > -1) {
            var args = Array.prototype.slice.call(arguments, 1);

            return this.each(
                function () {
                    var plugin = this['data-plugin-' + PLUGIN_NAME];

                    if (plugin) {
                        plugin[aOptions].apply(plugin, args);
                    }
                }
            );
        }

        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            this['data-plugin-' + PLUGIN_NAME] = new Toolbar(this, options);
        });
    };
    /**
     * toolbar plugin default options
     *
     * @type {{icons: string, type: string, selected: number,
     *          buttonTemplate: string, onSelect: Function, onDisable: Function}}
     */
    $.fn[PLUGIN_NAME].defaults = {
        /**
         * @enum {string} show|hide|only
         */
        icons:          'show',
        /**
         * @enum {string} default|radio|tab
         */
        type:           'default',
        selected:       0,
        buttonTemplate: '<a href="#{content}" name="{name}" class="{className}"><span>{label}</span></a>',
        onSelect:       FN_DUMMY,
        onDisable:      FN_DUMMY,
        onClick:        FN_DUMMY
    };
}
(jQuery, window, document));
