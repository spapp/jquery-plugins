/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.23.
 * @external  String
 */

/**
 * @constant
 * @type {number}
 */
String.PAD_LEFT = 0;
/**
 * @constant
 * @type {number}
 */
String.PAD_RIGHT = 1;
/**
 * @constant
 * @type {number}
 */
String.PAD_BOTH = 2;
/**
 * @constant
 * @type {string}
 * @default
 */
String.PAD_STRING = ' ';
/**
 * @constant
 * @type {number}
 * @default
 */
String.PAD_DIRECTION = String.PAD_RIGHT;
/**
 * Pad a string to a certain length with another string.
 *
 * <code>
 * var a = 'Alien';
 *
 * console.log(a.pad(10));              // returns 'Alien     '
 * console.log(a.padLeft(10, '-='));    // returns '-=-=-Alien'
 * console.log(a.padBoth(10, '_'));     // returns '__Alien___'
 * console.log(a.pad(6, '___'));        // returns 'Alien_'
 * </code>
 *
 * @link http://hu1.php.net/str_pad
 * @param {number} aPadLength
 * @param {string} [aPadString]
 * @param {number} [aPadType]
 * @returns {string}
 */
String.prototype.pad = function String_pad (aPadLength, aPadString, aPadType) {
    var methods = [
        'padLeft',
        'padRight',
        'padBoth'
    ];

    if ('number' !== typeof(aPadType) || aPadType < 0 || aPadType > 2) {
        aPadType = String.PAD_DIRECTION;
    }

    return this[methods[aPadType]](aPadLength, aPadString);
};
/**
 * Pad a string to a certain length with another string.
 *
 * @see String.prototype.pad
 * @link http://hu1.php.net/str_pad
 *
 * @param {number|string} inputString
 * @param {number} aPadLength
 * @param {string} [aPadString]
 * @param {number} [aPadType]
 * @returns {string}
 */
String.pad = function String_static_pad (aInputString, aPadLength, aPadString, aPadType) {
    // FIXME check type, NaN, Infinity
    // {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof#Examples}
    aInputString = aInputString + '';

    return aInputString.pad(aPadLength, aPadString, aPadType);
};

(function () {
    /**
     * Returns a specal length string from other string.
     *
     * @param {number} aPadLength
     * @param {string} aPadString
     * @returns {string}
     * @private
     */
    function getPadText (aPadLength, aPadString) {
        aPadString = aPadString || String.PAD_STRING;
        var padText = '';

        while (padText.length < aPadLength) {
            padText = aPadString + padText;
        }

        if (padText.length > aPadLength) {
            padText = padText.slice(0, aPadLength - padText.length);
        }

        return padText;
    }

    /**
     * Returns text part.
     *
     * @param {string} aText
     * @param {number} aPadLength
     * @returns {string}
     * @private
     */
    function getTextPart (aText, aPadLength) {
        if (aPadLength < 0) {// FIXME see the direction
            return aText.substr(aPadLength);
        }
        return aText;
    }

    /**
     * Pad a string on the left side.
     *
     * Equal:
     * <code>
     * 'string'.pad(10, String.PAD_STRING, String.PAD_LEFT);
     * </code>
     *
     * @param {number} aPadLength
     * @param {string} aPadString
     * @returns {string}
     */
    String.prototype.padLeft = function String_padLeft (aPadLength, aPadString) {
        return getPadText(aPadLength - this.length, aPadString) + getTextPart(this, aPadLength);
    };
    /**
     * Pad a string on the right side.
     *
     * Equal:
     * <code>
     * 'string'.pad(10, String.PAD_STRING, String.PAD_RIGHT);
     * </code>
     *
     * @param {number} aPadLength
     * @param {string} aPadString
     * @returns {string}
     */
    String.prototype.padRight = function String_padRight (aPadLength, aPadString) {
        return getTextPart(this, aPadLength) + getPadText(aPadLength - this.length, aPadString);
    };
    /**
     * Pad a string on the both side.
     *
     * Equal:
     * <code>
     * 'string'.pad(10, String.PAD_STRING, String.PAD_BOTH);
     * </code>
     *
     * @param {number} aPadLength
     * @param {string} aPadString
     * @returns {string}
     */
    String.prototype.padBoth = function String_padBoth (aPadLength, aPadString) {
        var lenghLeft = parseInt(this.length / 2, 10);
        var lenghRight = this.length - lenghLeft;

        return getPadText(lenghLeft, aPadString) + getTextPart(this, aPadLength) + getPadText(lenghRight, aPadString);
    };
}());
/**
 * Make a string's first character uppercase.
 *
 * @returns {string}
 */
String.prototype.ucfirst = function String_ucfirst () {
    return (this.substr(0, 1).toLocaleUpperCase() + this.substr(1));
};

/**
 * Make a string's first character uppercase.
 *
 * @see String.prototype.ucfirst
 * @param {*} aText
 * @returns {string}
 */
String.ucfirst = function String_static_ucfirst (aText) {
    aText = aText + '';
    return aText.ucfirst();
};
/**
 * Uppercase the first character of each word in a string
 *
 * @returns {string}
 * @function
 */
String.prototype.ucwords = function String_ucwords () {
    var text = this.split(' ');

    for (var i = 0; i < text.length; i++) {
        text[i] = text[i].ucfirst();
    }

    return text.join(' ');
};
/**
 * Uppercase the first character of each word in a string
 *
 * @see String.prototype.ucwords
 * @param {string} aText
 * @returns {*}
 * @function
 */
String.ucwords = function String_static_ucwords (aText) {
    aText = aText + '';

    return aText.ucwords();
};
/**
 * Returns formatted string
 *
 * @param {string} aFormat
 * @returns {string}
 */
String.format = function String_static_format (aFormat /*,arg0, arg1, ..., argN*/) {
    var args = Array.prototype.slice.call(arguments, 0);
    var text = args.shift();

    return aFormat.replace(/\{([0-9]+)\}/g, function (fill, index) {
        if ('undefined' !== typeof args[index]) {
            return args[index];
        }

        return fill;
    });
};

(function () {
    var CHARACTERS = 'abcdefghijklmnoprstuvz';
    var NUMBERS = '0123456789';
    var SPECIAL_CHARACTERS = '\'"+!%/=()~$,;.>-*_{}@&#<>|?';
    var counter = 0;

    /**
     * Returns a random number between min and max.
     *
     * @param {number} aMax
     * @param {number} [aMin]
     * @returns {number}
     */
    function random (aMax, aMin) {
        aMin = aMin || 0;

        return aMin + Math.floor(Math.random() * aMax);
    };
    /**
     * Returns a random id string
     *
     * @param {string} aPrefix
     * @returns {string}
     * @function
     */
    String.uniqueId = function String_static_uniqueId (aPrefix) {
        var id = 1000 * Date.now() + (++counter % 1000);

        return (aPrefix||'') + id.toString(36);
    };
    /**
     * Returns a random string.
     *
     * @param {number} [aLength] string length
     * @param {boolean} [aSpecialCharacters] if it is not FALSE then contains special characters
     * @param {boolean} [aLower] if it is not FALSE then contains lower case characters
     * @param {boolean} [aUpper] if it is not FALSE then contains upper case characters
     * @param {boolean} [aNumbers] if it is not FALSE then contains numbers
     * @returns {string}
     */
    String.pwgen = function String_static_pwgen (aLength, aSpecialCharacters, aLower, aUpper, aNumbers) {
        aLength = aLength || 8;

        var chars = '';
        var pw = '';

        if (false !== aLower) {
            chars += CHARACTERS.toLowerCase();
        }

        if (false !== aUpper) {
            chars += CHARACTERS.toUpperCase();
        }

        if (false !== aNumbers) {
            chars += NUMBERS;
        }

        if (true === aSpecialCharacters) {
            chars += SPECIAL_CHARACTERS;
        }

        while (aLength--) {
            pw += chars.charAt(random(chars.length - 1));
        }

        return pw;
    };

}());

if (!String.prototype.trim) {
    /**
     * The trim() method removes whitespace from both ends of the string.
     *
     * @returns {string}
     */
    String.prototype.trim = function String_trim () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

