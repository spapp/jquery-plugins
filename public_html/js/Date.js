/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @since     2015.04.23.
 * @external  Date
 */
if (!Date.now) {
    /**
     * Return a Number value that is the time value designating the UTC date and time of the occurrence of the call to
     * now.
     *
     * @see ECMA-262 5.1 Edition 15.9.4.4
     * @function
     * @returns {number}
     */
    Date.now = function Date_now () {
        return new Date().getTime();
    };
}

/**
 * Returns _TRUE_ if object is a Date.
 *
 * @param {*} aObj
 * @function
 * @returns {boolean}
 */
Date.isDate = function Date_isDate (aObj) {
    return (Object.prototype.toString.call(aObj) === '[object Date]');
};

/**
 * Mysql date time format.
 *
 * @type {string}
 * @constant
 */
Date.FORMAT_MYSQL_DATE_TIME = 'YYYY-MM-DD HH:mm:ss';

/**
 * ISO 8601 : 2004-02-12T15:19:21+00:00
 * @type {string}
 * @constant
 */
Date.FORMAT_ISO_8601 = 'YYYY-MM-DDTHH:mm:ssZ';

/**
 * RFC 2822 :  Thu, 21 Dec 2000 16:01:07 +0200
 * @type {string}
 * @constant
 */
Date.FORMAT_RFC_2822 = 'DDD, DD MMM YYYY HH:mm:ss ZZ';

/**
 * @type {string} locale string (english)
 * @constant
 */
Date.LOCALE_EN = 'en';

/**
 * @type {string} locale string (hungarian)
 * @constant
 */
Date.LOCALE_HU = 'hu';

/**
 * @type {string} default date time format
 * @constant
 * @default
 */
Date.DEFAULT_FORMAT = Date.FORMAT_MYSQL_DATE_TIME;

/**
 * @type {string} default locale
 * @constant
 * @default
 */
Date.DEFAULT_LOCALE = Date.LOCALE_EN;

(function () {
    var DAY_NAMES = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
    ];
    var SHORT_DAY_NAMES = [
        'Sun',
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Sat'
    ];
    var MONTH_NAMES = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    var SHORT_MONTH_NAMES = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sept',
        'Oct',
        'Nov',
        'Dec'
    ];
    var AMPM = [
        'am',
        'pm'
    ];

    var dictionary = {
        hu: {
            Sunday:    'vasárnap',
            Monday:    'hétfő',
            Tuesday:   'kedd',
            Wednesday: 'szerda',
            Thursday:  'csütörtök',
            Friday:    'péntek',
            Saturday:  'szombat',
            Sun:       'V',
            Mon:       'H',
            Tue:       'K',
            Wed:       'Sze',
            Thu:       'Cs',
            Fri:       'P',
            Sat:       'Szo',
            January:   'január',
            February:  'február',
            March:     'március',
            April:     'április',
            May:       'május',
            June:      'június',
            July:      'július',
            August:    'augusztus',
            September: 'szeptember',
            October:   'október',
            November:  'november',
            December:  'december',
            Jan:       'jan.',
            Feb:       'feb.',
            Mar:       'már.',
            Apr:       'ápr.',
            May:       'máj.',
            Jun:       'jún.',
            Jul:       'júl.',
            Aug:       'aug.',
            Sept:      'szept.',
            Oct:       'okt.',
            Nov:       'nov.',
            Dec:       'dec.',
            am:        'de.',
            pm:        'du.'
        }
    };

    /**
     * Returns translated string.
     *
     * If it is not exists then returns same.
     *
     * @param {string} text
     * @param {string} locale
     * @returns {string}
     * @private
     */
    function translate (text, locale) {
        if (locale in dictionary && text in dictionary[locale]) {
            text = dictionary[locale][text];
        }
        return text;
    }

    /**
     * Returns a part of date.
     *
     * Helper function to tokens.
     *
     * @param {string}  method
     * @param {number}  padLength
     * @param {Array}   fromValues
     * @param {number}  sleep
     * @param {Date}    date
     * @param {string}  locale
     * @param {boolean} isUtc
     * @returns {string}
     * @private
     */
    function getDatePart (method, padLength, fromValues, sleep, date, locale, isUtc) {
        var fn = 'get' + (isUtc ? 'UTC' : '') + method.ucfirst();
        var value = date[fn]() + (sleep ? sleep : 0);

        if (fromValues) {
            return translate(fromValues[value], locale);
        }

        return String.pad(value, padLength, '0', String.PAD_LEFT);
    }

    /**
     * Returns formatted timezone offset.
     *
     * Helper function to tokens.
     *
     * @param {string} format
     * @param {Date}   date
     * @returns {string}
     * @private
     */
    function getDateTimezoneOffset (format, date) {
        var d = new Date();
        var offset = Math.abs(date.getTimezoneOffset());
        var prefix = date.getTimezoneOffset() < 0 ? '-' : '+';

        d.setHours(parseInt(offset / 60, 10));
        d.setMinutes(parseInt(offset % 60, 10));

        return prefix + d.format(format);
    }

    /**
     * Returns formatted am or pm.
     *
     * Helper function to tokens.
     *
     * @param {boolean} isUpper
     * @param {Date}    date
     * @param {string}  locale
     * @param {boolean} isUtc
     * @returns {string}
     * @private
     */
    function getAmPm (isUpper, date, locale, isUtc) {
        var value = AMPM[parseInt(date.format('H', locale, isUtc) / 12, 10)];

        if (isUpper) {
            return value.toUpperCase();
        }

        return value.toLowerCase();
    }

    /**
     * 12-hour format of an hour without leading zeros.
     *
     * Helper function to tokens.
     *
     * @param {Date}    date
     * @param {string}  locale
     * @param {boolean} isUtc
     * @returns {number}
     * @private
     */
    function tokens_h (date, locale, isUtc) {
        var h = date.format('H', locale, isUtc);
        return h > 12 ? h - 12 : h;
    }

    /**
     * 12-hour format of an hour with leading zeros.
     *
     * Helper function to tokens.
     *
     * @param {Date}    date
     * @param {string}  locale
     * @param {boolean} isUtc
     * @returns {string}
     * @private
     */
    function tokens_hh (date, locale, isUtc) {
        return String.pad(tokens_h(date, locale, isUtc), 2, '0', String.PAD_LEFT)
    }

    /**
     * Tokens.
     *
     * @type {object}
     * @property {function} YYYY    Four digits representation of a year
     * @property {function} YY      Two digits representation of a year
     * @property {function} MMMM    Full textual representation of a month
     * @property {function} MMM     Short textual representation of a month
     * @property {function} MM      Numeric representation of a month with leading zeros
     * @property {function} M       Numeric representation of a month without leading zeros
     * @property {function} DDDD    Full textual representation of the day of the week
     * @property {function} DDD     Short textual representation of the day of the week
     * @property {function} DD      Day of the month with leading zeros
     * @property {function} D       Day of the month without leading zeros
     * @property {function} d       Numeric representation of the day of the week (0-6)
     * @property {function} HH      24-hour format of an hour with leading zeros
     * @property {function} H       24-hour format of an hour without leading zeros
     * @property {function} hh      12-hour format of an hour with leading zeros
     * @property {function} h       12-hour format of an hour without leading zeros
     * @property {function} mm      Minutes with leading zeros
     * @property {function} m       Minutes without leading zeros
     * @property {function} sss     Milliseconds with leading zeros
     * @property {function} ss      Seconds with leading zeros
     * @property {function} s       Seconds without leading zeros
     * @property {function} a       Lowercase Ante meridiem and Post meridiem
     * @property {function} A       Uppercase Ante meridiem and Post meridiem
     * @property {function} ZZ      Timezone offset (-0100)
     * @property {function} Z       Timezone offset (+02:00)
     */
    var tokens = {
        // Date - Year
        YYYY: getDatePart.bind(null, 'FullYear', 4, null, 0),
        YY:   getDatePart.bind(null, 'FullYear', -2, null, 0),
        // Date - Month
        MMMM: getDatePart.bind(null, 'month', 2, MONTH_NAMES, 0),
        MMM:  getDatePart.bind(null, 'month', 1, SHORT_MONTH_NAMES, 0),
        MM:   getDatePart.bind(null, 'month', 2, null, 1),
        M:    getDatePart.bind(null, 'month', 1, null, 1),
        // Date - Day
        DDDD: getDatePart.bind(null, 'day', 2, DAY_NAMES, 0),
        DDD:  getDatePart.bind(null, 'day', 1, SHORT_DAY_NAMES, 0),
        DD:   getDatePart.bind(null, 'date', 2, null, 0),
        D:    getDatePart.bind(null, 'date', 1, null, 0),
        d:    getDatePart.bind(null, 'day', 1, null, 0),
        // Time - hour
        HH:   getDatePart.bind(null, 'hours', 2, null, 0),
        H:    getDatePart.bind(null, 'hours', 1, null, 0),
        hh:   tokens_hh,
        h:    tokens_h,
        // Time - minute
        mm:   getDatePart.bind(null, 'minutes', 2, null, 0),
        m:    getDatePart.bind(null, 'minutes', 1, null, 0),
        // Time - second
        sss:  getDatePart.bind(null, 'milliseconds', 3, null, 0),
        ss:   getDatePart.bind(null, 'seconds', 2, null, 0),
        s:    getDatePart.bind(null, 'seconds', 1, null, 0),
        // Time - meridiem
        a:    getAmPm.bind(null, false),
        A:    getAmPm.bind(null, true),
        // TimezoneOffset
        ZZ:   getDateTimezoneOffset.bind(null, 'hhmm'),
        Z:    getDateTimezoneOffset.bind(null, 'hh:mm')
    };
    /**
     *
     * @type {RegExp}
     */
    var regexp = new RegExp(Object.keys(tokens).join('|'), 'g');
    /**
     * Returns formatted date/time string.
     *
     * Example:
     * <code>
     *     var d = new Date();
     *     d.format(Date.FORMAT_MYSQL_DATE_TIME);
     *
     *     // or
     *
     *     Date.format(new Date(), Date.FORMAT_MYSQL_DATE_TIME);
     *
     *     // or
     *
     *     Date.format(null, Date.FORMAT_MYSQL_DATE_TIME);
     * </code>
     *
     * @see tokens
     * @param {string}      [format]
     * @param {string}      [locale]
     * @param {boolean}     [isUtc]
     * @returns {string}
     */
    Date.prototype.format = function Date_format (format, locale, isUtc) {
        var _this = this;
        format = format || Date.DEFAULT_FORMAT;
        isUtc = isUtc || false;
        locale = locale || Date.DEFAULT_LOCALE;

        if (format === Date.FORMAT_ISO_8601 || format === Date.FORMAT_RFC_2822) {
            locale = Date.LOCALE_EN;
        }

        return format.replace(regexp, function (w) {
            if (w in tokens) {
                return tokens[w](_this, locale, isUtc);
            }
            return w;
        });
    };
    /**
     * Returns formatted date/time string.
     *
     * Example:
     * <code>
     *     var d = new Date();
     *     d.format(Date.FORMAT_MYSQL_DATE_TIME);
     *
     *     // or
     *
     *     Date.format(new Date(), Date.FORMAT_MYSQL_DATE_TIME);
     *
     *     // or
     *
     *     Date.format(null, Date.FORMAT_MYSQL_DATE_TIME);
     * </code>
     *
     * @see tokens
     * @param {string|Date} [date]
     * @param {string}      [format]
     * @param {string}      [locale]
     * @param {boolean}     [isUtc]
     * @returns {string}
     */
    Date.format = function Date_static_format (date, format, locale, isUtc) {
        if (!Date.isDate(date)) {
            date = Date.create(date);
        }

        return date.format(format, locale, isUtc);
    };
    /**
     * Creates a JavaScript Date instance.
     *
     * <code>
     *     new Date() === Date.create();
     * </code>
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
     * @param {*}      [year]
     * @param {number} [month]
     * @param {number} [day]
     * @param {number} [hour]
     * @param {number} [minute]
     * @param {number} [second]
     * @returns {Date}
     * @static
     */
    Date.create = function Date_static_create (year, month, day, hour, minute, second) {
        var date;

        if (Date.isDate(year)) {
            date = year;
        } else if (0 === arguments.length) {
            date = new Date();
        } else if (1 === arguments.length) {
            if ('number' === typeof year) {
                date = new Date(parseInt(year, 10));
            } else if ('string' === typeof year) {
                if (year.match(/^\d{1,2}(:\d{1,2}){1,2}$/)) {// only time
                    date = new Date(Date.create().format('YYYY-MM-DD ') + year);
                } else {
                    date = new Date(Date.parse(year));
                }
            } else {
                date = new Date();
            }
        } else {
            date = new Date(parseInt(year, 10),
                            parseInt(month || 0, 10),
                            parseInt(day || 0, 10),
                            parseInt(hour || 0, 10),
                            parseInt(minute || 0, 10),
                            parseInt(second || 0, 10)
            );
        }

        return date;
    };

    /**
     * Sets locale names.
     *
     * @see DAY_NAMES
     * @see SHORT_DAY_NAMES
     * @see MONTH_NAMES
     * @see SHORT_MONTH_NAMES
     * @see AMPM
     * @param {string}       locale
     * @param {string|Object} name
     * @param {string}       [value]
     */
    Date.setTranslate = function Date_setTranslate (locale, name, value) {
        if (!(locale in dictionary)) {
            dictionary[locale] = {};
        }

        if (Object.prototype.toString.call(name) === '[object Object]') {
            var names = Object.keys(name);

            for (var i = 0; i < names.length; i++) {
                Date.setTranslate(locale, names[i], name[names[i]]);
            }
        } else {
            dictionary[locale][name] = value;
        }
    };
}());
/**
 * Copies date and returns a new date object
 *
 * @returns {Date}
 * @function
 */
Date.prototype.clone = function Date_clone () {
    return Date.create(this.format());
};
/**
 * @see Date.prototype.clone
 * @returns {Date}
 * @function
 */
Date.clone = function Date_clone (aDate) {
    return aDate.clone();
};
/**
 * Returns date different
 *
 * @param {Date} aDateA
 * @param {Date} aDateB
 * @param {boolean} [aAsSecund]
 * @returns {number}
 * @function
 */
Date.diff = function Date_diff (aDateA, aDateB, aAsSecund) {
    var diff = Math.abs(Date.create(aDateA).getTime() - Date.create(aDateB).getTime());

    if (true === aAsSecund) {
        diff = diff / 1000;
    }

    return diff;
};
