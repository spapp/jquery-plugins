/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   billiard
 * @since     2015.04.28.
 */
/**
 *
 * @param {array|object} aData
 * @constructor
 */
var Collection;

(function () {
    var TYPE_ARRAY = '[object Array]';
    var TYPE_OBJECT = '[object Object]';

    /**
     * @param {array} aData
     * @returns {[]}
     */
    function parseArray (aData) {
        var data = [];
        var keys = [];

        for (var i = 0; i < aData.length; i++) {
            keys.push(i);
            data.push(
                {
                    name:  i,
                    value: aData[i]
                }
            );
        }

        return [
            keys,
            data
        ];
    }

    /**
     * @param {object} aData
     * @returns {[]}
     */
    function parseObject (aData) {
        var dataKeys = Object.keys(aData);
        var data = [];
        var keys = [];

        for (var i = 0; i < dataKeys.length; i++) {
            keys.push(dataKeys[i]);
            data.push(
                {
                    name:  dataKeys[i],
                    value: aData[dataKeys[i]]
                }
            );
        }

        return [
            keys,
            data
        ];
    }

    /**
     * Collection
     *
     * @param {array|object} aData
     * @constructor
     * @class
     */
    Collection = function Collection (aData) {
        var data;

        this._originalType = Object.prototype.toString.call(aData);

        switch (this._originalType) {
            case TYPE_ARRAY:
                data = data = parseArray(aData);
                break;
            case TYPE_OBJECT:
                data = data = parseObject(aData);
                break;
            default:
                throw new TypeError('Not supported type (' + type + ').');
        }

        this._keys = data[0];
        this._data = data[1];
    }
    /**
     * @type {array}
     * @private
     */
    Collection.prototype._data = null;
    /**
     * @type {array}
     * @private
     */
    Collection.prototype._keys = null;
    /**
     * @type {string}
     * @private
     */
    Collection.prototype._originalType = null;
    /**
     * If the given name can be found in the collection then returns it
     * otherwise returns the given default value.
     *
     * @param {string} aName
     * @param {*} aDefault
     * @returns {*|Collection}
     * @function
     */
    Collection.prototype.get = function Collection_get (aName, aDefault) {
        var index = this.indexOf(aName);

        if (index > -1) {
            aDefault = this._data[index].value;
        }

        switch (Object.prototype.toString.call(aDefault)) {
            case TYPE_ARRAY:
            case TYPE_OBJECT:
                aDefault = new Collection(aDefault);
                break;
        }

        return aDefault;
    };
    /**
     * @todo
     * @param {string} aName
     * @param {*} aValue
     * @function
     */
    Collection.prototype.set = function Collection_set (aName, aValue) {
    };
    /**
     * Returns TRUE if the a given name can be found in the collection
     *
     * @param {string} aName
     * @returns {boolean}
     * @function
     */
    Collection.prototype.has = function Collection_has (aName) {
        return (this.indexOf(aName) > -1);
    };
    /**
     * Returns the first index at which a given name can be found in the collection
     *
     * @param {string} aName
     * @returns {number}
     * @function
     */
    Collection.prototype.indexOf = function Collection_indexOf (aName) {
        return this._keys.indexOf(aName);
    };
    /**
     * Returns collection as an array
     *
     * @returns {Array}
     * @function
     */
    Collection.prototype.toArray = function Collection_toArray () {
        var data = [];

        for (var i = 0; i < this._data.length; i++) {
            data.push(this._data[i].value);
        }

        return data;
    };
    /**
     * Returns collection as an object
     *
     * @returns {object}
     * @function
     */
    Collection.prototype.toObject = function Collection_toObject () {
        var data = {};

        for (var i = 0; i < this._data.length; i++) {
            data[this._data[i].name] = this._data[i].value;
        }

        return data;
    };
}());
