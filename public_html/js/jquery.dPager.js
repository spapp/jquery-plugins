/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   jquery_plugins
 * @since     2015.02.26.
 *
 *
 * jQuery Discrete Pager plugin
 *
 * use static
 *
 * <code>
 *     // finds all elements with '.discrete-pager' class (defined: $.fn.dPager.defaults.selector)
 *     $.dPager('.discrete-pager');
 * </code>
 *
 * custom event handler
 *
 * <code>
 *     $('.discrete-pager').dPager(
 *          {
 *              // @param {jQuery.Event} event
 *              // @param {Pager}        pager
 *              // @param {Element}      pagerEl
 *              handler: function (event, pager, pagerEl) {
 *                  // do something
 *                  pager.load(); // loads the next page
 *              }
 *          }
 *      );
 * </code>
 *
 * global event
 *
 * <code>
 *     $.dPager(
 *          {
 *              handler: 'event'
 *          }
 *     );
 *
 *     // somewhere
 *     $(document).on(
 *          $.dPager.EVENT_PAGE,
 *
 *          // @param {jQuery.Event} event
 *          // @param {Pager}        pager
 *          // @param {Element}      pagerEl
 *          function (event, pager, pagerEl) {
 *              // do something
 *              pager.load(); // loads the next page
 *          }
 *     );
 *
 * </code>
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'dPager';

    /**
     * Discrete Pager class
     *
     * @param {jQuery|Element|string} aElement
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Pager (aElement, aOptions) {
        this._element = $(aElement);
        this._options = aOptions;
        this._document = $(document);
        this._window = $(window);
        this._container = $(this.getOption('container', this._element.attr('data-container')));
        this._loader = $('<div class="discrete-pager-loader">' + this.getOption('loaderText', '') + '</div>')
            .css(this.getOption('loaderStyle', {}))
            .hide()
            .appendTo(this._document.find('body'));

        this._element.hide();

        if (!this.getOption('url')) {
            this._options.url = this._element.attr('data-url') || window.location.origin + '/'
                                                                  + window.location.pathname;
        }

        if (!this.getOption('pageCount')) {
            this._options.pageCount = this._element.find(this.getOption('linkSelector', 'a')).length;
        }

        $(window).scroll(this.onWindowScroll.bind(this));

        this.appendContent('');
    }

    /**
     * onPage event name
     *
     * The onPage event is fired when the document scrolled down.
     *
     * @type {string}
     */
    Pager.EVENT_PAGE = 'discretePager:page';

    /**
     * Contains the instance config.
     *
     * @type {object}
     * @private
     */
    Pager.prototype._options = null;
    /**
     * Contains the document
     *
     * @type {jQuery}
     * @private
     */
    Pager.prototype._document = null;
    /**
     * Contains the window
     *
     * @type {jQuery}
     * @private
     */
    Pager.prototype._window = null;
    /**
     * Content container
     *
     * @type {jQuery}
     * @private
     */
    Pager.prototype._container = null;
    /**
     * Loader
     *
     * @type {jQuery}
     * @private
     */
    Pager.prototype._loader = null;
    /**
     * @type {string}
     * @private
     */
    Pager.prototype._url = null;
    /**
     * The current page.
     *
     * @type {number}
     * @private
     */
    Pager.prototype._currentPage = 1;
    /**
     * Contains the pager element.
     *
     * @type {jQuery}
     * @private
     */
    Pager.prototype._element = null;

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Pager.prototype.getOption = function Pager_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     * Appends new content to container
     *
     * @param {string} aContentHtml
     * @returns {Pager}
     * @function
     */
    Pager.prototype.appendContent = function Pager_prototype_appendContent (aContentHtml) {
        $(aContentHtml).appendTo(this._container);
        this.hideLoader();

        if (this._container.height() > this.getHeight()) {
            this.onWindowScroll();
        }

        return this;
    };
    /**
     * Loads the new page
     *
     * @param {number} page
     * @function
     */
    Pager.prototype.load = function Pager_prototype_load (page) {
        var _this = this;
        var url = this.getOption('url');
        var data = {};

        page = page || this._currentPage;
        data[this.getOption('dataAttr')] = page;
        data.pager = this._element.attr('id');

        $.ajax(
            {
                url:      url,
                data:     data,
                dataType: 'html',
                type:     'GET',
                success:  this.appendContent.bind(this),
                error:    function () {
                    --_this._currentPage;
                    _this.hideLoader();
                }
            }
        );
    };
    /**
     * Show the loader
     *
     * @function
     */
    Pager.prototype.showLoader = function Pager_prototype_showLoader () {
        this._loader.show();
    };
    /**
     * Hide the loader
     *
     * @function
     */
    Pager.prototype.hideLoader = function Pager_prototype_hideLoader () {
        this._loader.hide();
    };
    /**
     * Document onScroll event handler
     *
     * @function
     */
    Pager.prototype.onWindowScroll = function Pager_prototype_onWindowScroll () {
        if (!this._loader.is(':visible') && this._window.scrollTop() >= this.getHeight()) {
            var handler = this.getOption('handler', null);
            this.showLoader();
            ++this._currentPage;

            if ('function' === typeof handler) {
                handler.call(
                    this._element.get(0),
                    $.Event(Pager.EVENT_PAGE),
                    this,
                    this._element.get(0)
                );
            } else if ('event' === handler) {
                this._document.trigger(
                    $.Event(Pager.EVENT_PAGE),
                    [
                        this,
                        this._element.get(0)
                    ]
                );
            } else if (this._currentPage <= this.getOption('pageCount') || true === this.getOption('overflowAllowed')) {
                this.load();
            } else {
                this.hideLoader();
                --this._currentPage;
            }
        }
    };
    /**
     * Returns the document actuel height
     *
     * @returns {number}
     * @function
     */
    Pager.prototype.getHeight = function Pager_prototype_getHeight () {
        return this._document.outerHeight() - this._window.height() - this.getOption('tolerance', 0);
    };
    /**
     * dPager plugin
     *
     * @param {object} aOptions
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function jQuery_prototype_dPager (aOptions) {
        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            new Pager(this, options);
        });
    };
    /**
     * dPager plugin default options
     *
     * @type {{url: string, pageCount: number, dataAttr: string, selector: string, linkSelector: string, tolerance:
     *     number, loaderText: string, handler: null, loaderStyle: {opacity: string, position: string, bottom: string,
     *     left: string}}}
     */
    $.fn[PLUGIN_NAME].defaults = {
        url:             null,
        pageCount:       null,
        dataAttr:        'page',
        selector:        '.discrete-pager',
        linkSelector:    'a',
        overflowAllowed: false,
        tolerance:       50,
        loaderText:      'Loading...',
        handler:         null,
        loaderStyle:     {
            opacity:  '0.5',
            position: 'fixed',
            bottom:   '10px',
            left:     '10px'
        }
    };
    /**
     * dPager plugin static access
     *
     * @param {object} aOptions
     */
    $[PLUGIN_NAME] = function jQuery_dPager (aOptions) {
        var selector = $.fn[PLUGIN_NAME].defaults.selector;

        if ('string' === typeof aOptions) {
            selector = aOptions;
        } else if (aOptions && 'string' === typeof aOptions.selector) {
            selector = aOptions.selector;
        }

        $(selector)[PLUGIN_NAME](aOptions);

        $(document).on('DOMNodeInserted', function (event) {
            $(event.target).find(selector)[PLUGIN_NAME](aOptions);
        });
    };
    /**
     * dPager plugin onPage event name
     *
     * @type {string}
     */
    $[PLUGIN_NAME].EVENT_PAGE = Pager.EVENT_PAGE;

}(jQuery, window, document));
