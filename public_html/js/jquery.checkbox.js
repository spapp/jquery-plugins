/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   jquery_plugins
 * @since     2015.03.02.
 *
 *
 * jQuery checkbox plugin
 *
 * <code>
 *     $.checkbox();
 * </code>
 *
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'checkbox';
    var SELECTOR = 'input[type="checkbox"]';

    var VALUE_ON = 'on';
    var VALUE_OFF = 'off';

    var SIZE_SMALL = 'small';
    var SIZE_MEDIUM = 'medium';
    var SIZE_LARGE = 'large';

    var HTML_TEMPLATE = [
        '<span class="ui-checkbox">',
        '<span class="icon">&nbsp;</span>',
        '<span class="switch">&nbsp;</span>',
        '</span>'
    ].join('');

    /**
     * Checkbox class
     *
     * @param {jQuery|Element|string} aElement
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Checkbox (aElement, aOptions) {
        this._element = $(aElement);
        this._options = aOptions;

        var value = this.getOption('checked', false) ? VALUE_ON : VALUE_OFF;
        var size = this._element.attr('data-type');

        if (!size) {
            size = this.getOption('size', SIZE_SMALL);
        }

        this._checkbox = $(HTML_TEMPLATE)
            .insertAfter(this._element)
            .on('click', this.onClick.bind(this))
            .addClass(size);

        if (this._element.is(':checked')) {
            this._checkbox.addClass('checked');
            value = VALUE_ON;
        }

        if (this._element.is(':disabled')) {
            this._checkbox.addClass('disabled');
        }

        this._element.attr('type', 'hidden').attr('value', value).removeAttr('disabled').removeAttr('checked');
    }

    /**
     * Contains the instance config.
     *
     * @type {object}
     * @private
     */
    Checkbox.prototype._options = null;
    /**
     * Contains the pager element.
     *
     * @type {jQuery}
     * @private
     */
    Checkbox.prototype._element = null;
    /**
     * Checkbox element
     *
     * @type {jQuery}
     * @private
     */
    Checkbox.prototype._checkbox = null;

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Checkbox.prototype.getOption = function Checkbox_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    /**
     * Returns TRUE if the checkbox is checked
     *
     * @returns {bool}
     * @function
     */
    Checkbox.prototype.isChecked = function Checkbox_prototype_isChecked () {
        return this._checkbox.hasClass('checked');
    };
    /**
     * Returns TRUE if the checkbox is disabled
     *
     * @returns {bool}
     * @function
     */
    Checkbox.prototype.isDisabled = function Checkbox_prototype_isDisabled () {
        return this._checkbox.hasClass('disabled');
    };
    /**
     * Returns checkbox name
     *
     * @returns {string}
     * @function
     */
    Checkbox.prototype.getName = function Checkbox_prototype_getName () {
        return this._element.attr('name');
    };
    /**
     * Set checkbox value
     *
     * @param {*} aValue
     * @returns {Checkbox}
     * @function
     */
    Checkbox.prototype.setValue = function Checkbox_prototype_setValue (aValue) {
        var value = this.getOption('offValue', VALUE_OFF);

        if (true === aValue || aValue === this.getOption('onValue', VALUE_ON)) {
            value = this.getOption('onValue', VALUE_ON);
        }

        this._element.attr('value', value);

        return this;
    };
    /**
     * Returns checkbox current value
     *
     * @returns {*}
     * @function
     */
    Checkbox.prototype.getValue = function Checkbox_prototype_getValue () {
        if (true === this.isChecked()) {
            return this.getOption('onValue', VALUE_ON);
        }

        return this.getOption('offValue', VALUE_OFF);
    };
    /**
     * Checkbox onClick event
     *
     * @param {jQuery.Event} aEvent
     * @listens click
     * @fires document~checkbox:change
     * @function
     */
    Checkbox.prototype.onClick = function Checkbox_prototype_onClick (aEvent) {
        if (false === this.isDisabled()) {
            var callback = this.getOption('onChange');

            this._checkbox.toggleClass('checked');
            this.setValue(this.isChecked());

            if ('function' === typeof callback) {
                callback.call(this._checkbox.get(0), aEvent, this);
            }
            /**
             * document checkbox:change event
             *
             * @event document#checkbox:change
             * @property {jQuery.Event} aEvent - event object
             * @property {Checkbox}     aCheckbox - Checkbox class instance
             * @property {Element}      aInput - checkbox input element
             */
            $(document).trigger(
                $.Event('checkbox:change'),
                [
                    this,
                    this._element.get(0)
                ]
            );
        }
    };

    /**
     * checkbox plugin
     *
     * @param {object} aOptions
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function jQuery_prototype_checkbox (aOptions) {
        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            new Checkbox(this, options);
        });
    };
    /**
     * checkbox plugin default options
     *
     * @property {object}   defaults
     * @property {string}   [defaults.onValue=on]
     * @property {string}   [defaults.offValue=off]
     * @property {string}   [defaults.size=small]
     * @property {Function} [defaults.onChange]
     */
    $.fn[PLUGIN_NAME].defaults = {
        onValue:  VALUE_ON,
        offValue: VALUE_OFF,
        size:     SIZE_SMALL,
        /**
         * @callback options.onChange
         * @param {jQuery.Event} aEvent
         * @param {Checkbox} aCheckbox
         */
        onChange: function (aEvent, aCheckbox) {
        }
    };
    /**
     * checkbox plugin static access
     *
     * @param {object} aOptions
     */
    $[PLUGIN_NAME] = function jQuery_checkbox (aOptions) {
        $(SELECTOR)[PLUGIN_NAME](aOptions);

        $(document).on('DOMNodeInserted', function (event) {
            $(event.target).find(SELECTOR)[PLUGIN_NAME](aOptions);
        });
    };
}(jQuery, window, document));
