/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   jquery_plugins
 * @required  $.table()
 * @since     2015.04.14.
 */

(function ($, window, document) {
    var PLUGIN_NAME = 'reservation';

    /**
     * Reservation class
     *
     * @param {jQuery|Element|string} aElement
     * @param {object} aOptions
     * @class
     * @constructor
     */
    function Reservation (aElement, aOptions) {
        var _this = this;
        this._element = $(aElement);
        this._options = aOptions;
        this._data = this.getOption('data', []);

        this._element.addClass(Reservation.CSS_CLASS);

        this.update();
    }

    Reservation.PREFIX_ID = 'reservation-';
    Reservation.CSS_CLASS = 'ui-reservation';
    Reservation.CSS_TABLE_CLASS = 'ui-reservation-table';
    /**
     * @type {jQuery}
     * @private
     */
    Reservation.prototype._element = null;
    /**
     * @type {object}
     * @private
     */
    Reservation.prototype._options = null;
    Reservation.prototype._data = null;
    Reservation.prototype._tableSize = null;
    Reservation.prototype._colors = null;
    Reservation.prototype._reservations = null;
    Reservation.prototype._reservationsTpl = '<div class="reservation"><p>'
                                             + '<span class="times">{0} - {1}</span> <span class="label">{2}</span>'
                                             + '</p><div class="buttons">'
                                             + '<button data-action="remove" class="remove">×</button>'
                                             + '<button data-action="freeze" class="freeze">{3}</button>'
                                             + '</div></div>';

    /**
     * Returns a config value.
     * If it is not exists then returns the aDefaultValue.
     *
     * @param {string} aKey
     * @param {*} aDefaultValue
     * @returns {*}
     * @function
     */
    Reservation.prototype.getOption = function Reservation_prototype_getOption (aKey, aDefaultValue) {
        if (aKey in this._options) {
            aDefaultValue = this._options[aKey];
        }

        return aDefaultValue;
    };
    Reservation.prototype.mergeOptions = function Reservation_prototype_mergeOptions (aOptions) {
        this._options = $.extend({}, this._options, aOptions);

        return this;
    };
    /**
     * Updates reservation plugin
     *
     * @returns {Reservation}
     * @function
     */
    Reservation.prototype.update = function Reservation_prototype_update () {
        this._element.html('');
        this._createTable();

        var _this = this;
        var table = this._element.find('table');

        this._colors = null;
        this._reservations = [];
        this._tableSize = {
            height:    table.find('tr.row:first').height(),
            width:     table.width(),
            left:      table.position().left,
            top:       table.position().top,
            rowHeight: table.find('tr.row:first').height() + 1
        };

        this.refresh();

        return this;
    };
    /**
     * Refresh reservations
     *
     * @returns {Reservation}
     * @function
     */
    Reservation.prototype.refresh = function Reservation_prototype_refresh () {
        this._element.find('.reservation').remove();

        for (var i = 0; i < this._data.length; i++) {
            this.add(this._data[i]);
        }

        return this;
    };
    /**
     *
     * @param {Event} event
     * @returns {boolean}
     * @function
     */
    Reservation.prototype._reservationButtonsClick = function Reservation_prototype__reservationButtonsClick (event) {
        var button = $(event.target);
        var reservation = button.closest('.reservation');

        if (false === reservation.hasClass('disabled')) {
            var data = decodeData(reservation.attr('data-data'));
            var action = button.attr('data-action');
            var e = $.Event(action);

            if (!this._fireCallback(this.getOption('on' + action.ucfirst()), e, data)) {
                this[action](data);
            }
        }

        return false;
    };

    /**
     * Fires a callback function
     *
     * @param {function} aCallback
     * @param {Event} aEvent
     * @param {object} aData
     * @private
     *
     * @function
     */
    Reservation.prototype._fireCallback = function Reservation_prototype__fireCallback (aCallback, aEvent, aData) {
        if ('function' === typeof aCallback) {
            aCallback.call(this, aEvent, aData);
            return true;
        }

        return false;
    };
    /**
     * Adds a new reservation
     *
     * @param {object} aData
     * @returns {Reservation}
     * @function
     */
    Reservation.prototype.add = function Reservation_prototype_add (aData) {
        if (!this.isValid(aData) || this.isReserved(aData)) {
            return this;
        }

        var _this = this;
        var item = this._getReservationParams(aData);
        var tpl = String.format(
            this._reservationsTpl,
            Date.create(aData.from).format('HH:mm'),
            Date.create(aData.to).format('HH:mm'),
            aData.label,
            item.buttonLabel
        );

        item.data = aData;

        $(tpl).addClass((true === aData.disabled ? 'disabled' : ''))
            .css({
                     top:             item.top,
                     left:            item.left,
                     height:          item.height,
                     width:           item.width,
                     backgroundColor: this._getColor()
                 })
            .attr('id', item.id)
            .attr('data-data', encodeData(item))
            .insertBefore(this._element.children(':first'))
            .find('button').click(this._reservationButtonsClick.bind(this))
            .end()
            //.draggable({
            //               scope:         this.getOption('ddScope'),
            //               axis:          'y',
            //               handle:        'p',
            //               containment:   'parent',
            //               snap:          true,
            //               snapTolerance: this._tableSize.rowHeight - 3,
            //               zIndex:        10000,
            //               revert:        'invalid'
            //           })
            .resizable({
                           grid:    this._tableSize.rowHeight,
                           handles: 's',
                           stop:    function (event, ui) {
                               var part = (ui.size.height + 1) / _this._tableSize.rowHeight;
                               var data = _this.get(this);
                               var toTime = Date.create(data.data.from);

                               toTime.setMinutes(toTime.getMinutes() + part * _this.getOption('partition'));

                               if (_this.isReserved(data.data.from, toTime, '#' + this.id)) {
                                   $(this).css({height: ui.originalSize.height + 'px'});
                               } else {
                                   var e = $.Event('reservationresize');
                                   var eventData = {
                                       newTo:   toTime.format('HH:mm'),
                                       restore: (function (element, height) {
                                           return function () {
                                               element.css({height: height + 'px'});
                                           };
                                       }($(this), ui.originalSize.height)),
                                       data:    data.data
                                   };
                                   e.data = eventData;

                                   _this._fireCallback(_this.getOption('onReservationResize'), e, eventData);
                               }
                           }
                       });

        this._reservations.push(item);
        _this._fireCallback(_this.getOption('onAdd'), item);

        return this;
    };
    /**
     * Returns a reservation params
     *
     * @param {object} aData
     * @returns {{id: string, top: Number, left: Number, width: Number, count: Number}}
     * @function
     */
    Reservation.prototype._getReservationParams = function Reservation_prototype__getReservationParams (aData) {
        var partition = this.getOption('partition');
        var startTime = this.getOption('fromTime');
        var freezeButtonLabel = this.getOption('freezeButtonLabel');
        var params = {
            id:  Reservation.PREFIX_ID + Date.create(aData.from).format('HH-mm'),
            top: this._tableSize.top
                   + this._calculatePartitionCount(startTime, aData.from, partition) * this._tableSize.rowHeight,
            left:  this._tableSize.left,
            width: this._tableSize.width,
            count: this._calculatePartitionCount(aData.from, aData.to, partition)
        };

        params.height = params.count * this._tableSize.rowHeight - 1;

        if ('function' === typeof freezeButtonLabel) {
            params.buttonLabel = freezeButtonLabel.call(this, params, aData);
        } else {
            params.buttonLabel = freezeButtonLabel;
        }

        return params;
    };
    /**
     * Returns a hex color string
     *
     * @returns {string}
     * @function
     * @private
     */
    Reservation.prototype._getColor = function Reservation_prototype__getColor () {
        if (!this._colors || this._colors.length < 1) {
            this._colors = this.getOption('colors', '').split(' ');
        }

        return this._colors.shift();
    };
    /**
     *
     * @param {string|Date|number} aFromTime
     * @param {string|Date|number} aToTime
     * @param {number} aPartitionTime
     * @returns {number}
     * @private
     */
    Reservation.prototype._calculatePartitionCount = function (aFromTime, aToTime, aPartitionTime) {
        return Math.ceil(Date.diff(aFromTime, aToTime, true) / 60 / aPartitionTime);
    };
    /**
     * Creates a day table
     *
     * @returns {Reservation}
     * @function
     */
    Reservation.prototype._createTable = function Reservation_prototype__createTable () {
        var _this = this;
        var data = new DayParts(
            this.getOption('fromTime'),
            this.getOption('toTime'),
            this.getOption('partition')
        );
        var headers = [
            {
                name:   'time',
                label:  'Time',
                format: function (aValue, aRowData) {
                    return aRowData.from + ' - ' + aRowData.to;
                }
            },
            {
                name:   'name',
                label:  'Név',
                format: function (aValue, aRowData) {
                    return '';
                }
            }
        ];

        $('<div></div>')
            .addClass(Reservation.CSS_TABLE_CLASS)
            .appendTo(this._element)
            .table(
            {
                scope:      this.getOption('ddScope'),
                showHeader: false,
                headers:    headers,
                data:       data.toArray(),
                droppable:  true,
                selectable: false,
                onDrop:     function (event, data) {
                    if (!_this.isReserved(data.target.data)) {
                        var e = $.Event('reservation');
                        e.data = data;

                        _this._fireCallback(_this.getOption('onReservation'), e, data);
                    }

                    return false;
                }
            }
        );

        return this;
    };
    /**
     * Sets reservation data and updata this plugin
     *
     * @param {array} aData
     * @returns {Reservation}
     * @function
     */
    Reservation.prototype.setData = function Reservation_prototype_setData (aData) {
        this._data = aData;
        this.update();

        return this;
    };
    /**
     * Returns TRUE if the pairs of times is a valid reservation
     *
     * @param {string|number|Date} aFrom
     * @param {string|number|Date} aTo
     * @returns {boolean}
     * @function
     */
    Reservation.prototype.isValid = function Reservation_prototype_isValid (aFrom, aTo) {
        var fromTime = Date.create(this.getOption('fromTime')).getTime();
        var toTime = Date.create(this.getOption('toTime')).getTime();

        if ('string' !== typeof aFrom && 'undefined' === typeof aTo) {
            aTo = aFrom.to;
            aFrom = aFrom.from;
        }

        if (aTo === '00:00') {
            aTo = '23:59';
        }

        aTo = Date.create(aTo).getTime();
        aFrom = Date.create(aFrom).getTime();

        return (aFrom >= fromTime && aTo <= toTime)
    };
    /**
     * Returns TRUE if the pairs of times is reserved
     *
     * @param {string|number|Date} aFrom
     * @param {string|number|Date} aTo
     * @param {string} aWithout
     * @returns {boolean}
     * @function
     */
    Reservation.prototype.isReserved = function Reservation_prototype_isReserved (aFrom, aTo, aWithout) {
        var selector = '.reservation';
        var reserved = false;
        var _this = this;

        if ('string' === typeof aWithout) {
            selector += ':not(' + aWithout + ')';
        }

        if ('string' !== typeof aFrom && 'undefined' === typeof aTo) {
            aTo = aFrom.to;
            aFrom = aFrom.from;
        }

        if (aTo === '00:00') {
            aTo = '23:59';
        }

        aTo = Date.create(aTo).getTime();
        aFrom = Date.create(aFrom).getTime();

        this._element.find(selector).each(function () {
            var data = _this.get(this).data;
            var from = Date.create(data.from).getTime();
            var to = Date.create(data.to).getTime();

            if ((aFrom > from && aFrom < to)
                || (aTo > from && aTo < to)
                || (aFrom < from && aTo >= to)
                || (aFrom > from && aTo <= to)) {
                reserved = true;
            }
        });

        return reserved;
    };
    /**
     * Returns a reservation data
     *
     * @param {string|number|Date} aFrom
     * @returns {object}
     * @function
     */
    Reservation.prototype.get = function Reservation_prototype_get (aFrom) {
        var reservation = this.getElement(aFrom);
        var data = null;

        if (1 === reservation.length) {
            data = decodeData(reservation.attr('data-data'))
        }

        return data;
    };
    /**
     * Returns a reservation element
     *
     * @param {string|number|Date} aFrom
     * @returns {object}
     * @function
     */
    Reservation.prototype.getElement = function Reservation_prototype_getElement (aFrom) {
        if ('string' === typeof aFrom) {
            aFrom = '#' + Reservation.PREFIX_ID + Date.create(aFrom).format('HH-mm');
        }

        return $(aFrom);
    }
    /**
     * Removes a reservation element
     *
     * @param {string|number|Date} aFrom
     * @returns {object}
     * @function
     */
    Reservation.prototype.remove = function Reservation_prototype_remove (aFrom) {
        this.getElement(aFrom).remove();

        return this;
    };
    /**
     * Freezes a reservation
     *
     * @param {string|number|Date} aFrom
     * @returns {object}
     * @function
     */
    Reservation.prototype.freeze = function Reservation_prototype_freeze (aFrom) {
        this.getElement(aFrom).addClass('disabled');

        return this;
    };
    /**
     * Clears all reservation
     *
     * @returns {Reservation}
     * @function
     */
    Reservation.prototype.clear = function Reservation_prototype_clear () {
        this.setData([]);

        return this;
    };

    /**
     * @param {string|number|Date} aFromTime
     * @param {string|number|Date} aToTime
     * @param {number} aPartition
     * @constructor
     */
    function DayParts (aFromTime, aToTime, aPartition) {
        this._from = Date.create(aFromTime);
        this._to = Date.create(aToTime);
        this._partition = aPartition;
    }

    DayParts.prototype._from = null;
    DayParts.prototype._to = null;
    DayParts.prototype._current = null;
    DayParts.prototype._partition = null;
    /**
     * Returns table data as an array
     *
     * @returns {Array}
     * @function
     */
    DayParts.prototype.toArray = function DayPart_prototype_toArray () {
        var data = [];
        var item;

        this._current = this._from.clone();

        while (this._isValidDate()) {
            item = {
                from: this._current.format('HH:mm')
            };

            this._current.setMinutes(this._current.getMinutes() + this._partition);

            item.to = this._current.format('HH:mm');
            data.push({data: item});
        }

        return data;
    };
    /**
     * Returns TRUE if the current time is a valid time
     *
     * @returns {boolean}
     * @private
     */
    DayParts.prototype._isValidDate = function () {
        return (this._current.getFullYear() === this._to.getFullYear()
                && this._current.getMonth() === this._to.getMonth()
                && this._current.getDate() === this._to.getDate()
                && this._current.getHours() <= this._to.getHours());
    };
    /**
     *
     * @param {object} aData
     * @returns {string}
     */
    function encodeData (aData) {
        return encodeURI(JSON.stringify(aData));
    }

    /**
     *
     * @param {string} aData
     * @returns {*}
     */
    function decodeData (aData) {
        return JSON.parse(decodeURI(aData));
    }

    /**
     * reservation plugin
     *
     * @function update ()
     * @function add (aData)
     * @function setData (aData)
     * @function isValid (aFrom, aTo)
     * @function isReserved (aFrom, aTo, aWithout)
     * @function get (aFrom)
     * @function remove (aFrom)
     * @function freeze (aFrom)
     * @function clear ()
     * @param {object} aOptions
     * @param {*} aValue
     * @returns {*}
     */
    $.fn[PLUGIN_NAME] = function (aOptions, aValue) {
        var allowed = [
            'update',
            'refresh',
            'add',
            'setData',
            'isValid',
            'isReserved',
            'get',
            'remove',
            'freeze',
            'clear',
            'mergeOptions'
        ];

        if ('string' === typeof aOptions && $.inArray(aOptions, allowed) > -1) {
            var args = Array.prototype.slice.call(arguments, 1);

            return this.each(
                function () {
                    var plugin = this['data-plugin-' + PLUGIN_NAME];
                    plugin[aOptions].apply(plugin, args);
                }
            );
        }

        var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, aOptions);

        return this.each(function () {
            this['data-plugin-' + PLUGIN_NAME] = new Reservation(this, options);
        });
    };
    /**
     * reservation plugin default options
     *
     */
    $.fn[PLUGIN_NAME].defaults = {
        fromTime:            '00:00',
        toTime:              '23:59',
        ddScope:             'ui-reservation',
        colors:              '#B3E0BC #FFF3B6 #F7B3B3 #B3DEF6 #FFD4B3 #B3EAD5 #C1D3F8 #D5C3E1 #C7B5B3 #B3B3B3',
        partition:           60,
        freezeButtonLabel:   'Fizet',
        onReservation:       null,
        onRemove:            null,
        onFreeze:            null,
        onAdd:               null,
        onReservationResize: null

    };
}
(jQuery, window, document));

