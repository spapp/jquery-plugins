/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   jquery_plugins
 * @since     2015.03.13.
 * @link      https://bitbucket.org/spapp/jquery_plugins
 *
 * @see http://tools.ietf.org/html/rfc6531
 * @see http://tools.ietf.org/html/rfc5321
 * @see http://tools.ietf.org/html/rfc3696
 * @see http://en.wikipedia.org/wiki/Email_address
 *
 * @see http://tools.ietf.org/html/rfc6761
 * @see http://tools.ietf.org/html/rfc1591
 *
 * <code>
 *   var email = $.email();
 *
 *   email.setDomain('example.com').setLocalPart('john.doe');
 *
 *   console.log('mailto:' + email); // mailto:john.doe@example.com
 *
 * </code>
 *
 * <code>
 *   var email = $.email('john.doe@example.com');
 *
 *   if(true === email.isValid()){
 *      console.log('ok');
 *   } else {
 *      console.log('fail');
 *   }
 *
 * </code>
 *
 */
(function ($, window, document) {
    var PLUGIN_NAME = 'email';

    /**
     * Email class
     *
     * Builds a e-mail address and checks validity
     *
     * @param {string} aEmail
     * @class
     * @constructor
     */
    function Email (aEmail) {
        this.setEmail(aEmail || '');
    }

    Email.SIGN_AT = '@';
    Email.SIGN_DOT = '.';
    Email.PATTERN_LOCAL_PART = /^[a-z0-9_~!$&'(\[\])*+,\-;=.:!#$%*/?^`{|}~" <>]{1,64}$/ig;
    Email.PATTERN_IP_ADDRESS = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g;
    Email.PATTERN_DOMAIN = /^([a-z0-9][a-z0-9\-]*(\.[a-z0-9][a-z0-9\-]*)*){1,253}$/g;

    /**
     * Email local part
     *
     * @type {string}
     * @private
     */
    Email.prototype._localPart = '';
    /**
     * Email domain part
     *
     * @type {string}
     * @private
     */
    Email.prototype._domain = '';
    /**
     * Returns TRUE if the e-mail address is valid.
     *
     * @returns {boolean}
     * @function
     */
    Email.prototype.isValid = function Email_prototype_isValid () {
        return (this.isValidLocalPart() && this.isValidDomain() && this.getEmail().length <= 256);
    };
    /**
     * Returns TRUE if the local part of e-mail address is valid.
     *
     * @returns {boolean}
     * @function
     */
    Email.prototype.isValidLocalPart = function Email_prototype_isValidLocalPart () {
        return Email.PATTERN_LOCAL_PART.test(this.getLocalPart());
    };
    /**
     * Returns TRUE if the domain part of e-mail address is valid.
     *
     * @returns {boolean}
     * @function
     */
    Email.prototype.isValidDomain = function Email_prototype_isValidDomain () {
        return (Email.PATTERN_IP_ADDRESS.test(this.getDomain()) || Email.PATTERN_DOMAIN.test(this.getDomain()));
    };
    /**
     * Returns e-mail address local part
     *
     * @returns {string}
     * @function
     */
    Email.prototype.getLocalPart = function Email_prototype_getLocalPart () {
        return this._localPart;
    };
    /**
     * Sets up e-mail address local part
     *
     * @param {string} aLocalPart
     * @returns {Email}
     * @function
     */
    Email.prototype.setLocalPart = function Email_prototype_setLocalPart (aLocalPart) {
        this._localPart = aLocalPart;

        return this;
    };
    /**
     * Returns e-mail address domain part
     *
     * @returns {string}
     * @function
     */
    Email.prototype.getDomain = function Email_prototype_getDomain () {
        return this._domain;
    };
    /**
     * Sets up e-mail address domain part
     *
     * @param {string} aDomain
     * @returns {Email}
     * @function
     */
    Email.prototype.setDomain = function Email_prototype_setDomain (aDomain) {
        this._domain = aDomain;

        return this;
    };
    /**
     * Returns tld of domain part (last part of domain)
     *
     * @returns {string}
     * @function
     */
    Email.prototype.getTld = function Email_prototype_getTld () {
        var parts = this.getDomain().split(Email.SIGN_DOT);

        if (parts.length > 1) {
            return parts.pop();
        }

        return '';
    };
    /**
     * Sets up tld of domain part
     *
     * @param {string} aTld
     * @returns {Email}
     * @function
     */
    Email.prototype.setTld = function Email_prototype_setTld (aTld) {
        var parts = this.getDomain().split(Email.SIGN_DOT);

        parts.pop();
        parts.push(aTld);
        this.setDomain(parts.join(Email.SIGN_DOT));

        return this;
    };
    /**
     * Returns e-mail address
     *
     * @returns {string}
     * @function
     */
    Email.prototype.getEmail = function Email_prototype_getEmail () {
        return this.getLocalPart() + Email.SIGN_AT + this.getDomain();
    };
    /**
     * Sets up e-mail address
     *
     * @param {string} aEmail
     * @returns {Email}
     * @function
     */
    Email.prototype.setEmail = function Email_prototype_setEmail (aEmail) {
        var parts = aEmail.split(Email.SIGN_AT);

        this.setLocalPart(parts.shift());
        this.setDomain(parts.join(Email.SIGN_AT));

        return this;
    };

    /**
     * Returns the email address as a string.
     * Magice function.
     *
     * @returns {string}
     * @function
     */
    Email.prototype.toString = function Email_prototype_toString () {
        return this.getEmail();
    };

    /**
     * email plugin static access
     *
     * @param {string} aEmail
     * @returns {Email}
     */
    $[PLUGIN_NAME] = function jQuery_email (aEmail) {
        return new Email(aEmail);
    };
}(jQuery, window, document));













































